﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

float r;
float g;
float b;

Texture2D inputT2D;
sampler2D inputSampler2D = sampler_state
{
	Texture = <inputT2D>;
	AddressU = Border;
	AddressV = Border;
};

//Texture2D myT2D;
//sampler2D mySampler2D = sampler_state
//{
//	Texture = <myT2D>;
//};

struct VertexShaderOutput
{
	//float4 Position : SV_POSITION;	
	float4 Color : COLOR0;						// имеющийся цвет обрабатываемой точки
	float2 TexCoord : TEXCOORD0;			// позиция пикселя на текстуре, который нужно наложить
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float4 color = tex2D(inputSampler2D, input.TexCoord);

	
	return color * input.Color * float4(r,g,b,1);
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};