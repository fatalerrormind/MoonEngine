﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using MoonEngine;
using MoonEngine.Components;
using MoonEngine.Test;
using System.Drawing;
using System.Windows.Forms;

namespace GameStart
{
    class MainGame : Game
    {
        Core core;
        GraphicsDeviceManager gdm;
        Form1 form1;

        public MainGame()
        {
            gdm = new GraphicsDeviceManager(this);
            gdm.PreferredBackBufferWidth = Settings.DefPreferredBackBufferWidth;
            gdm.PreferredBackBufferHeight = Settings.DefPreferredBackBufferHeight;
            gdm.IsFullScreen = Settings.DefIsFullScreen;
            gdm.SynchronizeWithVerticalRetrace = Settings.DefSynchronizeWithVerticalRetrace;
            gdm.ApplyChanges();

            // 83333 ticks - 120 fps
            //TargetElapsedTime = new TimeSpan(83333);
            //TargetElapsedTime = new TimeSpan(1000000);
            //TargetElapsedTime = new TimeSpan(2000000);
            IsFixedTimeStep = false;
            IsMouseVisible = true;
            Window.IsBorderless = false;
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += Window_ClientSizeChanged;

            // Замена системного курсора
            Bitmap cur = new Bitmap(@"Content\windows_cursor_normal.png");
            IntPtr ptr = cur.GetHicon();
            Cursor c = new Cursor(ptr);
            System.Windows.Forms.Form.FromHandle(this.Window.Handle).Cursor = c;
        }

        private void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            core.Forms.InvalidateLayout();
            core.Forms.InvalidateBuffer();
        }

        // Загрузить контент
        protected override void LoadContent()
        {
            Content.RootDirectory = "Content";
            //Fonts.Load();
            //Textures.Load();
        }

        // Инициализация
        protected override void Initialize()
        {
            base.Initialize();
            core = new Core(this, gdm);
            form1 = new Form1(core);
            core.Forms.Add(form1);
            core.Forms.Add(new Form2(core));
        }
        
        // Выгрузить контент, выход из игры
        //protected override void UnloadContent()
        //{ }

        // Обновление игровых данных, столкновения, перемащение, воспроизведение
        protected override void Update(GameTime gameTime)
        {
            if (core.Input.IsKeyUpDown(Microsoft.Xna.Framework.Input.Keys.Escape))
                Exit();
            core.Update(gameTime);
            base.Update(gameTime);
        }

        // Отрисовка игровых объектов
        protected override void Draw(GameTime gameTime)
        {
            core.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
