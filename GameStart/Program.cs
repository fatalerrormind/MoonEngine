﻿using System;
using GameStart.Code;

namespace GameStart
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var mg = new MainGame())
                mg.Run();
        }
    }
#endif
}
