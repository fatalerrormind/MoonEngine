﻿namespace MoonEngine
{
    public struct Margin
    {
        public int Right;
        public int Bottom;
        public int Left;
        public int Top;

        public Margin(int right, int bottom, int left, int top)
        {
            Right = right;
            Bottom = bottom;
            Left = left;
            Top = top;
        }
        public Margin(int hor, int vert)
        {
            Right = hor;
            Bottom = vert;
            Left = hor;
            Top = vert;
        }
        public Margin(int padding)
        {
            Right = padding;
            Bottom = padding;
            Left = padding;
            Top = padding;
        }
        public static Margin Zero
        { get { return new Margin(0); } }

        public static Margin operator +(Margin obj1, Margin obj2)
        {
            return new Margin(obj1.Right + obj2.Right, obj1.Bottom + obj2.Bottom, obj1.Left + obj2.Left, obj1.Top + obj2.Top);
        }
        public static Margin operator -(Margin obj1, Margin obj2)
        {
            return new Margin(obj1.Right - obj2.Right, obj1.Bottom - obj2.Bottom, obj1.Left - obj2.Left, obj1.Top - obj2.Top);
        }
        public static bool operator ==(Margin obj1, Margin obj2)
        {
            return obj1.Right == obj2.Right && obj1.Bottom == obj2.Bottom && obj1.Left == obj2.Left && obj1.Top == obj2.Top;
        }
        public static bool operator !=(Margin obj1, Margin obj2)
        {
            return obj1.Right != obj2.Right || obj1.Bottom != obj2.Bottom || obj1.Left != obj2.Left || obj1.Top != obj2.Top;
        }
        //public override bool Equals(object obj)
        //{
        //    return base.Equals(obj);
        //}
        //public override int GetHashCode()
        //{
        //    return base.GetHashCode();
        //}
    }
}
