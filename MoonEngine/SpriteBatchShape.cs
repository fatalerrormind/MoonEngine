﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MoonEngine
{
    public class SpriteBatchShape : SpriteBatch
    {
        public Texture2D Point { get; }

        public SpriteBatchShape(GraphicsDevice graphicsDevice, Texture2D point) : base(graphicsDevice)
        {
            Point = point;
        }

        public void DrawRectangleСontour(int posX, int posY, int sizeX, int sizeY, int lineWidth, Color color)
        {
            Draw(Point, new Rectangle(posX, posY, sizeX, lineWidth), color);
            Draw(Point, new Rectangle(posX + sizeX - lineWidth, posY, lineWidth, sizeY), color);
            Draw(Point, new Rectangle(posX, posY + sizeY - lineWidth, sizeX, lineWidth), color);
            Draw(Point, new Rectangle(posX, posY, lineWidth, sizeY), color);
        }
        public void DrawRectangleСontour(Point pos, Point size, int lineWidth, Color color)
        {
            Draw(Point, new Rectangle(pos.X, pos.Y, size.X, lineWidth), color);
            Draw(Point, new Rectangle(pos.X + size.X - lineWidth, pos.Y, lineWidth, size.Y), color);
            Draw(Point, new Rectangle(pos.X, pos.Y + size.Y - lineWidth, size.X, lineWidth), color);
            Draw(Point, new Rectangle(pos.X, pos.Y, lineWidth, size.Y), color);
        }
        public void DrawRectangleСontour(Rectangle rect, int lineWidth, Color color)
        {
            Draw(Point, new Rectangle(rect.X, rect.Y, rect.Width, lineWidth), color);
            Draw(Point, new Rectangle(rect.X + rect.Width - lineWidth, rect.Y, lineWidth, rect.Height), color);
            Draw(Point, new Rectangle(rect.X, rect.Y + rect.Height - lineWidth, rect.Width, lineWidth), color);
            Draw(Point, new Rectangle(rect.X, rect.Y, lineWidth, rect.Height), color);
        }

        public void DrawRectangleFill(int posX, int posY, int sizeX, int sizeY, Color color)
        {
            Draw(Point, new Rectangle(posX, posY, sizeX, sizeY), color);
        }
        public void DrawRectangleFill(Point pos, Point size, Color color)
        {
            Draw(Point, new Rectangle(pos.X, pos.Y, size.X, size.Y), color);
        }
        public void DrawRectangleFill(Rectangle rect, Color color)
        {
            Draw(Point, new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), color);
        }


        //public static void Line(SpriteBatch sb, int p1X, int p1Y, int p2X, int p2Y, int lineWidth, Color color)
        //{
        //    sb.Draw(Point, new Rectangle(posX, posY, sizeX, sizeY), color);
        //}
        //public static void Line(SpriteBatch sb, Point p1, Point p2, int lineWidth, Color color)
        //{

        //    float distance = Vector2.Distance(p1.ToVector2(), p2.ToVector2());
        //    Rectangle source = new Rectangle(0, 0, (int)distance, lineWidth);
        //    //float angle = GetAngleInDegrees(Point1, Point2);
        //    //float center = Center(Point1, Point2);
        //    sb.Draw(Shape.Point, p1, source, Color.Black, MathHelper.ToRadians(45), Vector2.Zero,
        //        1f, SpriteEffects.None, 0f);



        //    sb.Draw(Point, new Rectangle(pos.X, pos.Y, size.X, size.Y), color);
        //}
        //public static void Line(SpriteBatch sb, Rectangle rect, int lineWidth, Color color)
        //{
        //    sb.Draw(Point, new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), color);
        //}
    }
}
