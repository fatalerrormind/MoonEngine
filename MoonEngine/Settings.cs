﻿using Microsoft.Xna.Framework;

namespace MoonEngine
{
    public class Settings
    {
        public Core Core { get; private set; }

        public Settings(Core core)
        {
            Core = core;
        }

        #region GraphicsDeviceManager
        public const int DefPreferredBackBufferWidth = 800;
        public const int DefPreferredBackBufferHeight = 600;
        public const bool DefIsFullScreen = false;
        public const bool DefSynchronizeWithVerticalRetrace = true;

        public void SetParamsGdm(int preferredBackBufferWidth, int preferredBackBufferHeight, bool isFullScreen, bool synchronizeWithVerticalRetrace)
        {
            Core.Gdm.PreferredBackBufferWidth = preferredBackBufferWidth;
            Core.Gdm.PreferredBackBufferHeight = preferredBackBufferHeight;
            Core.Gdm.IsFullScreen = isFullScreen;
            Core.Gdm.SynchronizeWithVerticalRetrace = synchronizeWithVerticalRetrace;
            Core.Gdm.ApplyChanges();
            Core.Forms.InvalidateLayout();
        }

        public void SetPreferredBackBuffer(int preferredBackBufferWidth, int preferredBackBufferHeight)
        {
            Core.Gdm.PreferredBackBufferWidth = preferredBackBufferWidth;
            Core.Gdm.PreferredBackBufferHeight = preferredBackBufferHeight;
            Core.Gdm.ApplyChanges();
            Core.Forms.InvalidateLayout();
        }

        public void SetIsFullScreen(bool isFullScreen)
        {
            Core.Gdm.IsFullScreen = isFullScreen;
            Core.Gdm.ApplyChanges();
            Core.Forms.InvalidateLayout();
        }

        public void SetSynchronizeWithVerticalRetrace(bool synchronizeWithVerticalRetrace)
        {
            Core.Gdm.SynchronizeWithVerticalRetrace = synchronizeWithVerticalRetrace;
            Core.Gdm.ApplyChanges();
            Core.Forms.InvalidateLayout();
        }
        #endregion
    }
}
