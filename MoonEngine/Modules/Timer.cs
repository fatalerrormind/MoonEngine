﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Modules
{
    public class Timer
    {
        public float Interval { get; set; } = 1f;
        public float Counter { get; set; } = 0f;
        public bool OnOverflow { get; set; } = false;
        public bool IsEnabled { get; set; } = false;

        public void Update(float deltaTime)
        {
            if (IsEnabled)
            {
                Counter += deltaTime;

                if (Counter >= Interval)
                {
                    OnOverflow = true;
                    Counter = 0f;
                }
                else
                    OnOverflow = false;
            }
            else
                OnOverflow = false;
        }

        public void Start()
        {
            IsEnabled = true;
        }

        public void Pause()
        {
            IsEnabled = false;
        }

        public void Stop()
        {
            IsEnabled = false;
            Counter = 0f;
        }
    }
}
