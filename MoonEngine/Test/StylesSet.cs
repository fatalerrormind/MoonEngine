﻿using Microsoft.Xna.Framework;
using MoonEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Test
{
    static public class StylesSet
    {
        static public Label.LabelStyle Label1;
        static public Button.ButtonStyle Button1;
        static public Button.ButtonStyle Button2;
        static public Button.ButtonStyle Button3;
        static public ScrollBar.ScrollBarStyle ScrollBar1;
        static public ScrollPanel.ScrollPanelStyle ScrollPanel1;

        static public void Init(Core core)
        {
            Label1 = new Label.LabelStyle(core);
            Label1.ClearColor = new Color(0, 0, 0, 60);
            Label1.FontColor = Color.White;

            Button1 = new Button.ButtonStyle(core);
            Button1.ColorNormal = Color.Transparent;
            Button1.ColorHover = Color.OrangeRed;
            Button1.ColorPressed = Color.Gray;
            Button1.MaskIsVisible = false;
            Button1.FrameIsVisible = false;

            Button2 = new Button.ButtonStyle(core);
            Button2.ColorNormal = Color.Transparent;
            Button2.ColorHover = Color.CadetBlue;
            Button2.ColorPressed = Color.Gray;
            Button2.MaskIsVisible = false;
            Button2.FrameIsVisible = false;

            Button3 = new Button.ButtonStyle(core);
            Button3.ColorHover = Color.CadetBlue;
            Button3.ColorPressed = Color.White;
            Button3.MaskIsVisible = false;
            Button3.FrameIsVisible = false;

            ScrollBar1 = new ScrollBar.ScrollBarStyle(core);
            ScrollBar1.ButtonMinusStyle = Button3;
            ScrollBar1.ButtonPlusStyle = Button3;
            ScrollBar1.ButtonThumbStyle = Button3;

            ScrollPanel1 = new ScrollPanel.ScrollPanelStyle(core);
            ScrollPanel1.ScrollBarHorizontalStyle = ScrollBar1;
            ScrollPanel1.ScrollBarVerticalStyle = ScrollBar1;
            ScrollPanel1.StubStyle = Button3;
        }
    }
}
