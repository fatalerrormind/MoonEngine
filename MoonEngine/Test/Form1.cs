﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MoonEngine.Components;
using MoonEngine.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Test
{
    public class Form1 : Form
    {
        StackPanel spMainPanel;
        Button btnfs, btnsh1h, btnsh1v;
        Button btnRemoveAll, btnCreateForm;
        StackPanel spTest;
        ScrollPanel scpHistory;
        TextBlock tbHistory;

        ListBox listBox;
        Button listBoxPos, listBoxSize;

        GridTemplate gridTemplate;

        string history1 = "История 1\n__________\nЛожь во благо\nПрестарелому отцу парня требовались для передвижения ходунки, но старик считал, что с ними он будет выглядеть \"инвалидом\", поэтому парень подсунул ему состряпанный самостоятельно рекламный листок со \"спортивными ходунками\", и тот с радостью согласился на покупку.\n\nВ текст он вставил фразы:\n- Изготовлены для все еще подвижных, активных, любящих гулять пожилых людей...\n- Ходунки имеют малый вес и оборудованы мягким сидением со спинкой, повторяющей форму спины, благодаря чему можно, при необходимости, присесть и отдохнуть...\n- Прочно фиксируются в разложенном положении для дополнительной безопасности...\n\nПРЕДУПРЕЖДЕНИЕ: Эти ходунки изготовлены для энергичных пожилых людей, способных бодро передвигаться самостоятельно.Если вы инвалид, недееспособны или у вас существуют какие - либо ограничения, то это изделие не для вас!";
        string history2 = "\n\nИстория 2\n__________\nИстория про неожиданный повод для гордости\nИду на почту по дворам. Трое мелких мальчишек играют в футбол. Один пнул мяч слишком сильно, так что тот укатился на дорожку передо мной.\n- А подкиньте мячик, -кричат мальчишки.\nЯ разгоняюсь и неожиданно даже для себя ловко пинаю мячик в их сторону. Первый, и, возможно, единственный раз я не просто попала по мячу, да еще и пнула его с достаточной силой и в нужную сторону.\n- Спасиибо!-слышу вслед стройный хор.\n- Видишь, Тох, даже девушка бьет лучше тебя.";

        public Form1(Core core) : base(core)
        {
            StylesSet.Init(Core);
            ClearColor = Color.CornflowerBlue;

            spMainPanel = new StackPanel(Core);
            spMainPanel.ClearColor = Color.FromNonPremultiplied(0, 0, 0, 40);
            spMainPanel.HorizontalAlignment = HorizontalAlignment.Right;
            spMainPanel.VerticalAlignment = VerticalAlignment.Bottom;
            Bind(spMainPanel);

            btnCreateForm = new Button(Core, "Create Form2", null, StylesSet.Button2);
            btnCreateForm.Size = new Point(100, 20);
            btnCreateForm.Margin = new Margin(2);
            btnCreateForm.IsVisualReactionIsRightDown = false;
            spMainPanel.Bind(btnCreateForm);

            btnRemoveAll = new Button(Core, "Remove All", null, StylesSet.Button1);
            btnRemoveAll.Size = new Point(100, 20);
            btnRemoveAll.Margin = new Margin(2);
            spMainPanel.Bind(btnRemoveAll);

            btnsh1h = new Button(Core, "H.sh1");
            btnsh1h.Size = new Point(50, 20);
            btnsh1h.Margin = new Margin(2);
            spMainPanel.Bind(btnsh1h);

            btnsh1v = new Button(Core, "V.sh1");
            btnsh1v.Size = new Point(50, 20);
            btnsh1v.Margin = new Margin(2);
            spMainPanel.Bind(btnsh1v);

            btnfs = new Button(Core, "FullScreen");
            btnfs.Size = new Point(100, 20);
            btnfs.Margin = new Margin(2);
            spMainPanel.Bind(btnfs);
            
            spTest = new StackPanel(Core);
            spTest.Orientation = Orientation.Vertical;
            spTest.AutoSizeHorizontal = false;
            spTest.AutoSizeVertical = false;
            spTest.Pos = new Point(10, 30);
            spTest.Size = new Point(300, 500);
            Bind(spTest);

            for (int i = 0; i < 10; i++)
            {
                Label label = new Label(Core, "Строка " + i, StylesSet.Label1);
                spTest.Bind(label);
            }

            scpHistory = new ScrollPanel(Core);
            //scpHistory.Pos = new Point(390, 30);
            //scpHistory.Size = new Point(200, 200);
            scpHistory.Pos = new Point(30, 30);
            scpHistory.Size = new Point(700, 500);
            scpHistory.CanCatchMouse = true;
            //scpHistory.Stub.SetStyle(StylesSet.Button3);
            Bind(scpHistory);

            tbHistory = new TextBlock(Core);
            tbHistory.Font = Core.sf_system;
            tbHistory.Text = history1;
            tbHistory.ClearColor = new Color(0, 0, 0, 30);
            scpHistory.Component = tbHistory;

            //Button btn = new Button(Core, "ЖМЯК");
            //btn.Size = new Point(250);
            //scpHistory.Component = btn;

            //gridTemplate = new GridTemplate(Core);
            //gridTemplate.Pos = new Point(100);
            //gridTemplate.Orientation = Orientation.Vertical;
            //gridTemplate.HorizontalAlignment = HorizontalAlignment.Center;
            //gridTemplate.VerticalAlignment = VerticalAlignment.Center;
            //gridTemplate.GridSize = 5;
            //gridTemplate.CellSize = new Point(50, 20);

            listBox = new ListBox(Core);
            listBox.ClearColor = new Color(0, 0, 0, 50);
            listBox.Pos = new Point(100);
            listBox.Size = new Point(200);
            listBox.CellHeight = 30;
            listBox.HorizontalAlignment = HorizontalAlignment.Center;
            Bind(listBox);

            listBoxPos = new Button(Core, "P");
            listBoxPos.Size = new Point(16);

            listBoxSize = new Button(Core, "S");
            listBoxSize.Size = new Point(16);

            listBox.Bind(listBoxPos);
            listBox.Bind(listBoxSize);

            for (int i = 0; i < 10000; i++)
            {
                Label label = new Label(Core, "Яч. " + i);
                label.ClearColor = Color.Black;
                label.FontColor = Color.White;
                listBox.Bind(label);
                //Bind(label);
                //gridTemplate.Components.Add(label);

                //Button btn = new Button(Core, "Яч. " + i);
                //btn.Size = new Point(RandomHelper.Int(50, 300), RandomHelper.Int(20, 30));
                //listBox.Bind(btn);
            }

            //listBox.SelectedIndex = 10;
        }

        override public void Update()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Update();
            
            if (this == Core.Control.LeftClicked)
            {
                tbHistory.Text += history2;
            }
            
            if (btnCreateForm == Core.Control.LeftClicked)
                Core.Forms.Add(new Form2(Core));
            if (btnCreateForm == Core.Control.RightClicked)
            {
                Form2 f1 = new Form2(Core);
                f1.Size = new Point(300, 400);
                Core.Forms.Add(f1);
                Form2 f2 = new Form2(Core);
                f2.Pos = new Point(30, 30);
                f2.Size = new Point(150, 200);
                f1.Bind(f2);
            }
            else if (btnRemoveAll == Core.Control.LeftClicked)
                for (int i = 1; i < Core.Forms.Count;)
                    Core.Forms.Remove(i, true);
            else if (btnsh1h == Core.Control.LeftClicked)
            {
                if (spTest.HorizontalAlignment == HorizontalAlignment.Left)
                    spTest.HorizontalAlignment = HorizontalAlignment.Center;
                else if (spTest.HorizontalAlignment == HorizontalAlignment.Center)
                    spTest.HorizontalAlignment = HorizontalAlignment.Right;
                else
                    spTest.HorizontalAlignment = HorizontalAlignment.Left;
            }
            else if (btnsh1v == Core.Control.LeftClicked)
            {
                if (spTest.VerticalAlignment == VerticalAlignment.Top)
                    spTest.VerticalAlignment = VerticalAlignment.Center;
                else if (spTest.VerticalAlignment == VerticalAlignment.Center)
                    spTest.VerticalAlignment = VerticalAlignment.Bottom;
                else
                    spTest.VerticalAlignment = VerticalAlignment.Top;
            }
            else if (btnfs == Core.Control.LeftClicked)
            {
                if (Core.Gdm.IsFullScreen)
                    Core.Settings.SetParamsGdm(Settings.DefPreferredBackBufferWidth, Settings.DefPreferredBackBufferHeight, false, Settings.DefSynchronizeWithVerticalRetrace);
                else
                    Core.Settings.SetParamsGdm(1366, 768, true, Settings.DefSynchronizeWithVerticalRetrace);
            }

            if (scpHistory == Core.Control.OnMouse)
            {
                if (Core.Input.IsKeyDown(Keys.LeftShift) || Core.Input.IsKeyDown(Keys.RightShift))
                    scpHistory.ScrollBarHorizontal.DataCurrent += Core.Input.MouseDeltaWheel() * scpHistory.ScrollBarHorizontal.Step;
                else
                    scpHistory.ScrollBarVertical.DataCurrent += Core.Input.MouseDeltaWheel() * scpHistory.ScrollBarVertical.Step;
            }
            if (scpHistory.Stub == Core.Control.LeftDown)
            {
                scpHistory.Size -= Core.Input.MouseDeltaPos();
            }


            if (listBoxPos == Core.Control.LeftDown)
            {
                listBox.Pos -= Core.Input.MouseDeltaPos();
            }

            if (listBoxSize == Core.Control.LeftDown)
            {
                listBox.Size -= Core.Input.MouseDeltaPos();
            }
        }


        public override void Layout()
        {
            if (IsInvalidateLayout)
            {
                tbHistory.Size = new Point(scpHistory.Size.X - scpHistory.ScrollBarVertical.Size.X, tbHistory.Size.Y);
            }

            for (int i = 0; i < Components.Count; i++)
                Components[i].Layout();

            if (IsInvalidateLayout)
            {
                // Расчёт шаблона
                Size = Core.ScreenSize;
                Pos = Point.Zero;
                spMainPanel.Pos = Size - spMainPanel.Size;
                //tbHistory.Layout();
                //gridTemplate.Calc();

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                // Отрисовка под UI
                // ...
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Draw();
                // Отрисовка над UI
                // ...
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
