﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Components;
using MoonEngine.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Test
{
    public class Form2 : Form
    {
        Button btnClose;
        Button btnHide;
        Button btnDrag;
        Button btn1, btn2, btn3, btn4, btn5, btn6, btn7;
        StackPanel sv1;
        ScrollPanel sc1;
        float r = 0f;

        public Form2(Core core) : base(core)
        {
            r = RandomHelper.Float(0f, 5f);
            ClearColor = RandomHelper.ColorRGB();
            Size = new Point(200, 300);
            Pos = new Point(Core.ScreenWidth / 2 - Size.X / 2, Core.ScreenHeight / 2 - Size.Y / 2);

            btnDrag = new Button(Core, "Menu");
            btnDrag.ColorNormal = Color.White;
            btnDrag.ColorPressed = Color.Gray;
            btnDrag.MaskIsVisible = false;
            btnDrag.FrameIsVisible = false;
            btnDrag.IsVisualReactionIsLeftDown = true;
            btnDrag.IsVisualReactionIsRightDown = false;
            btnDrag.IsVisualReactionOnMouse = false;
            
            btnClose = new Button(Core, "x");
            btnClose.ColorNormal = Color.Transparent;
            btnClose.ColorHover = Color.OrangeRed;
            btnClose.ColorPressed = Color.Gray;
            btnClose.MaskIsVisible = false;
            btnClose.FrameIsVisible = false;

            btnHide = new Button(Core, "_");
            btnHide.ColorNormal = Color.Transparent;
            btnHide.ColorHover = Color.CadetBlue;
            btnHide.ColorPressed = Color.Gray;
            btnHide.MaskIsVisible = false;
            btnHide.FrameIsVisible = false;

            Bind(btnDrag);
            Bind(btnHide);
            Bind(btnClose);
            
            sc1 = new ScrollPanel(Core);
            sc1.ScrollBarHorizontal.AutoIsEnabled = false;
            sc1.ScrollBarHorizontal.IsEnabled = false;
            sc1.ScrollBarHorizontal.IsVisible = false;
            sc1.Stub.IsVisible = false;
            Bind(sc1);

            sv1 = new StackPanel(Core);
            sv1.Orientation = Orientation.Vertical;
            sv1.HorizontalAlignment = HorizontalAlignment.Center;
            sv1.VerticalAlignment = VerticalAlignment.Center;
            sv1.AutoSizeHorizontal = false;
            sv1.AutoSizeVertical = true;
            //sv1.ClearColor = Color.FromNonPremultiplied(0, 0, 0, 20);
            sc1.Component = sv1;

            btn1 = new Button(Core, "Hor");
            btn1.Size = new Point(150, 20);
            btn1.Margin = new Margin(4);
            sv1.Bind(btn1);

            btn2 = new Button(Core, "Ver");
            btn2.Size = new Point(150, 20);
            btn2.Margin = new Margin(4);
            sv1.Bind(btn2);

            btn3 = new Button(Core, "Toggle FS");
            btn3.Size = new Point(150, 20);
            btn3.Margin = new Margin(4);
            sv1.Bind(btn3);

            btn4 = new Button(Core, "Toggle Bg");
            btn4.Size = new Point(150, 20);
            btn4.Margin = new Margin(4);
            sv1.Bind(btn4);

            btn5 = new Button(Core, "Toggle Stub");
            btn5.Size = new Point(150, 20);
            btn5.Margin = new Margin(4);
            sv1.Bind(btn5);

            btn6 = new Button(Core, "Toggle ScHor");
            btn6.Size = new Point(150, 20);
            btn6.Margin = new Margin(4);
            sv1.Bind(btn6);

            btn7 = new Button(Core, "Toggle ScVer");
            btn7.Size = new Point(150, 20);
            btn7.Margin = new Margin(4);
            sv1.Bind(btn7);

            for (int i = 0; i < 10; i++)
            {
                Label label = new Label(Core, "Строка " + i);
                //label.Style.FontColor = RandomHelper.ColorRGB();
                sv1.Bind(label);
            }
        }

        override public void Update()
        {
            if (btnDrag == Core.Control.LeftUpDown || this == Core.Control.LeftUpDown)
                Core.Forms.ToFront(this);

            if (btnDrag == Core.Control.LeftDown)
                Pos -= Core.Input.MouseDeltaPos();

            if (IsEnabled)
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Update();

            if (btnClose == Core.Control.LeftClicked)
                Core.Forms.Remove(this, true);

            if (btnHide == Core.Control.LeftClicked)
                IsVisible = false;

            if (btn1 == Core.Control.LeftClicked)
            {
                if (sv1.HorizontalAlignment == HorizontalAlignment.Left)
                    sv1.HorizontalAlignment = HorizontalAlignment.Center;
                else if (sv1.HorizontalAlignment == HorizontalAlignment.Center)
                    sv1.HorizontalAlignment = HorizontalAlignment.Right;
                else
                    sv1.HorizontalAlignment = HorizontalAlignment.Left;
            }
            else if (btn2 == Core.Control.LeftClicked)
            {
                if (sv1.VerticalAlignment == VerticalAlignment.Top)
                    sv1.VerticalAlignment = VerticalAlignment.Center;
                else if (sv1.VerticalAlignment == VerticalAlignment.Center)
                    sv1.VerticalAlignment = VerticalAlignment.Bottom;
                else
                    sv1.VerticalAlignment = VerticalAlignment.Top;
            }
            else if(btn3 == Core.Control.LeftClicked)
            {
                if (Core.Gdm.IsFullScreen)
                    Core.Settings.SetParamsGdm(Settings.DefPreferredBackBufferWidth, Settings.DefPreferredBackBufferHeight, false, Settings.DefSynchronizeWithVerticalRetrace);
                else
                    Core.Settings.SetParamsGdm(1366, 768, true, Settings.DefSynchronizeWithVerticalRetrace);
            }
            else  if (btn4 == Core.Control.LeftClicked)
            {
                ClearColor = RandomHelper.ColorRGB();
            }
            else if (btn5 == Core.Control.LeftClicked)
            {
                sc1.Stub.IsVisible = !sc1.Stub.IsVisible;
            }
            else if (btn6 == Core.Control.LeftClicked)
            {
                sc1.ScrollBarHorizontal.IsVisible = !sc1.ScrollBarHorizontal.IsVisible;
            }
            else if (btn7 == Core.Control.LeftClicked)
            {
                sc1.ScrollBarVertical.IsVisible = !sc1.ScrollBarVertical.IsVisible;
            }

            if (sc1.Stub == Core.Control.LeftDown)
            {
                Size -= Core.Input.MouseDeltaPos();
            }
        }

        public override void Layout()
        {
            // Выставляем компонентам актуальные параметры
            if (IsInvalidateLayout)
            {
                btnDrag.Pos = new Point(0, 0);
                btnDrag.Size = new Point(Size.X, 20);
                btnClose.Pos = new Point(Size.X - 20, 0);
                btnClose.Size = new Point(20, 20);
                btnHide.Pos = new Point(Size.X - 20 - btnClose.Size.X, 0);
                btnHide.Size = new Point(20, 20);
                sc1.Pos = new Point(1, btnDrag.Size.Y);
                sc1.Size = new Point(Size.X - 2, Size.Y - btnDrag.Size.Y - 1);
                sv1.Size = new Point(sc1.Size.X - sc1.ScrollBarVertical.Size.X, sv1.Size.Y);
            }

            // Обновляем шаблоны компонентов
            for (int i = 0; i < Components.Count; i++)
                Components[i].Layout();

            if (IsInvalidateLayout)
            {
                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }

        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                // Отрисовка под UI
                // ...
                Core.Sb.DrawRectangleСontour(Point.Zero, Size, 1, Color.White);
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Draw();
                // Отрисовка над UI
                // ...
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }

        //public override void Draw()
        //{
        //    if (IsVisible)
        //    {
        //        Rectangle sourceCorrect = Rectangle.Empty;

        //        if (Source == Rectangle.Empty)
        //            sourceCorrect = new Rectangle(0, 0, Size.X, Size.Y);
        //        else
        //        {
        //            sourceCorrect = Source;
        //            if (sourceCorrect.X >= Size.X) sourceCorrect.X = Size.X - 1; // коррекция позиции соурса
        //            if (sourceCorrect.Y >= Size.Y) sourceCorrect.Y = Size.Y - 1;
        //            if (sourceCorrect.X + sourceCorrect.Width > Size.X) sourceCorrect.Width = Size.X - sourceCorrect.X; // коррекция размера соурса
        //            if (sourceCorrect.Y + sourceCorrect.Height > Size.Y) sourceCorrect.Height = Size.Y - sourceCorrect.Y;
        //        }

        //        //Core.Sb.Draw(Buffer, Pos.ToVector2(), sourceCorrect, IsEnabled ? EnabledTone : DisabledTone);
        //        Core.Sb.Draw(Buffer, Pos.ToVector2(), sourceCorrect, IsEnabled ? EnabledTone : DisabledTone,
        //            r, new Vector2(0), 1f, SpriteEffects.None, 0f);
        //    }
        //}
    }
}
