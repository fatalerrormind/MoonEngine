﻿using Microsoft.Xna.Framework;
using MoonEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Templates
{
    abstract public class Template
    {
        #region Свойства
        public Core Core { get; set; } //ядро
        public Point Pos { get; set; } = Point.Zero; //позиция рабочей области
        #endregion

        // Конструктор
        public Template(Core core)
        {
            Core = core;
        }

        // Просчёт шаблона, нужно вызывать из секции if (IsInvalidateLayout)
        abstract public void CalcSize();
        abstract public void CalcPos();
    }
}
