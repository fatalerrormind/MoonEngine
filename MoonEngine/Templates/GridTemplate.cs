﻿using Microsoft.Xna.Framework;
using MoonEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Templates
{
    /// <summary>
    /// Шаблон располагает компоненты в виде сетки. Количеством колонок или строк, взависимости от ориентации, фиксировано. Размер ячейки так же фиксирован
    /// При горизонтальной ориентации количество строк фиксировано, заполнение происходит по столбцам, сверху вниз, слева направо.
    /// При вертикальной ориентации количество столбцов фиксировано, заполнение происходит по строкам, слева направо, сверху вниз.
    /// </summary>
    public class GridTemplate
    {
        #region Свойства
        public Point Pos { get; set; } = Point.Zero; // позиция рабочей области
        public List<Component> Components { get; set; } = new List<Component>(); // лист компонентов
        public Orientation Orientation { get; set; } = Orientation.Horizontal; // ориентация сетки, направление нарастания сетки
        public HorizontalAlignment HorizontalAlignment { get; set; } = HorizontalAlignment.Left; // выравнивание внутри ячейки по горизонтали
        public VerticalAlignment VerticalAlignment { get; set; } = VerticalAlignment.Top; // выравнивание компонента внутри ячейки по вертикали
        public Point CellSize { get; set; } = new Point(50, 10); // размер ячейки
        public int GridSize { get; set; } = 1; // количество колонок (при вертикальной) или строк (при горизонтальной)
        #endregion
        
        public void CalcPos()
        {
            for (int i = 0, y = 0, x = 0; i < Components.Count; i++) // проходим по всем компонентам, i-компонент, x-колонка, y-строка
            {
                Component component = Components[i];

                if (HorizontalAlignment == HorizontalAlignment.Left)
                    component.Pos = new Point(Pos.X + CellSize.X * x + component.Margin.Left, component.Pos.Y);
                else if (HorizontalAlignment == HorizontalAlignment.Center)
                    component.Pos = new Point(Pos.X + CellSize.X * x + CellSize.X / 2 - (component.Margin.Left + component.Size.X + component.Margin.Right) / 2, component.Pos.Y);
                else if (HorizontalAlignment == HorizontalAlignment.Right)
                    component.Pos = new Point(Pos.X + CellSize.X * x + CellSize.X - component.Size.X - component.Margin.Right, component.Pos.Y);

                if (VerticalAlignment == VerticalAlignment.Top)
                    component.Pos = new Point(component.Pos.X, Pos.Y + CellSize.Y * y + component.Margin.Top);
                else if (VerticalAlignment == VerticalAlignment.Center)
                    component.Pos = new Point(component.Pos.X, Pos.Y + CellSize.Y * y + CellSize.Y / 2 - (component.Margin.Top + component.Size.Y + component.Margin.Bottom) / 2);
                else if (VerticalAlignment == VerticalAlignment.Bottom)
                    component.Pos = new Point(component.Pos.X, Pos.Y + CellSize.Y * y + CellSize.Y - component.Size.Y - component.Margin.Bottom);

                if (Orientation == Orientation.Horizontal)
                {
                    y++;
                    if (y >= GridSize)
                    {
                        y = 0;
                        x++;
                    }
                }
                else
                {
                    x++;
                    if (x >= GridSize)
                    {
                        x = 0;
                        y++;
                    }
                }
            }
        }

        public Point GetCellPos(int index)
        {
            int x;
            int y;

            if (Orientation == Orientation.Horizontal)
            {
                x = index / GridSize;
                y = index % GridSize;
            }
            else
            {
                y = index / GridSize;
                x = index % GridSize;
            }
            return new Point(x * CellSize.X, y * CellSize.Y);
        }
        public Rectangle GetCellRectangle(int index)
        {
            int x;
            int y;

            if (Orientation == Orientation.Horizontal)
            {
                x = index / GridSize;
                y = index % GridSize;
            }
            else
            {
                y = index / GridSize;
                x = index % GridSize;
            }
            return new Rectangle(x * CellSize.X, y * CellSize.Y, CellSize.X, CellSize.Y);
        }

        //public int GetFirstVisibleIndex(Rectangle source)
        //{
        //    int x, y;

        //    if (Orientation == Orientation.Horizontal)
        //    {
        //        x = CellSize.X;
        //    }
        //    cellViewStart.Y = (int)((Camera.Pos.Y - Camera.Size.Y / Camera.Zoom * 0.5f) / cellSize.Y - 1);

        //    if (cellViewStart.X < 0) cellViewStart.X = 0;
        //    else if (cellViewStart.X > gl.Map.Size.X) cellViewStart.X = gl.Map.Size.X;

        //    if (cellViewStart.Y < 0) cellViewStart.Y = 0;
        //    else if (cellViewStart.Y > gl.Map.Size.Y) cellViewStart.Y = gl.Map.Size.Y;
        //}
        //public int GetLastVisibleIndex(Rectangle source)
        //{

        //    cellViewStart.X = (int)((Camera.Pos.X - Camera.Size.X / Camera.Zoom * 0.5f) / cellSize.X / 0.75f - 1);
        //    cellViewStart.Y = (int)((Camera.Pos.Y - Camera.Size.Y / Camera.Zoom * 0.5f) / cellSize.Y - 1);
        //    cellViewEnd.X = (int)((Camera.Pos.X + Camera.Size.X / Camera.Zoom * 0.5f) / cellSize.X / 0.75f + 1);
        //    cellViewEnd.Y = (int)((Camera.Pos.Y + Camera.Size.Y / Camera.Zoom * 0.5f) / cellSize.Y + 1);

        //    if (cellViewStart.X < 0) cellViewStart.X = 0;
        //    else if (cellViewStart.X > gl.Map.Size.X) cellViewStart.X = gl.Map.Size.X;
        //    if (cellViewEnd.X < 0) cellViewEnd.X = 0;
        //    else if (cellViewEnd.X > gl.Map.Size.X) cellViewEnd.X = gl.Map.Size.X;

        //    if (cellViewStart.Y < 0) cellViewStart.Y = 0;
        //    else if (cellViewStart.Y > gl.Map.Size.Y) cellViewStart.Y = gl.Map.Size.Y;
        //    if (cellViewEnd.Y < 0) cellViewEnd.Y = 0;
        //    else if (cellViewEnd.Y > gl.Map.Size.Y) cellViewEnd.Y = gl.Map.Size.Y;
        //}
    }
}
