﻿using Microsoft.Xna.Framework;
using MoonEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Templates
{
    public class StackTemplate
    {
        #region Свойства
        public Point Pos { get; set; } = Point.Zero; // позиция рабочей области
        public Point Size { get; set; } = new Point(1, 1); //размер рабочей области
        public List<Component> Components { get; set; } = new List<Component>();
        public Orientation Orientation { get; set; } = Orientation.Horizontal;
        public HorizontalAlignment HorizontalAlignment { get; set; } = HorizontalAlignment.Left;
        public VerticalAlignment VerticalAlignment { get; set; } = VerticalAlignment.Top;
        #endregion

        public void CalcPos()
        {
            Point allSize = new Point(0);

            for (int i = 0; i < Components.Count; i++)
            {
                Point curSize = new Point(
                    Components[i].Margin.Left + Components[i].Size.X + Components[i].Margin.Right,
                    Components[i].Margin.Top + Components[i].Size.Y + Components[i].Margin.Bottom);

                allSize += curSize;
            }

            int p = 0;

            if (Orientation == Orientation.Horizontal)
            {
                if (HorizontalAlignment == HorizontalAlignment.Left)
                    p = Pos.X;
                else if (HorizontalAlignment == HorizontalAlignment.Center)
                    p = Pos.X + Size.X / 2 - allSize.X / 2;
                else if (HorizontalAlignment == HorizontalAlignment.Right)
                    p = Pos.X + Size.X - allSize.X;

                if (VerticalAlignment == VerticalAlignment.Top)
                {
                    for (int i = 0; i < Components.Count; i++)
                    {
                        p += Components[i].Margin.Left;
                        Components[i].Pos = new Point(p, Pos.Y + Components[i].Margin.Top);
                        p += Components[i].Size.X + Components[i].Margin.Right;
                    }
                }
                else if (VerticalAlignment == VerticalAlignment.Center)
                {
                    for (int i = 0; i < Components.Count; i++)
                    {
                        p += Components[i].Margin.Left;
                        Components[i].Pos = new Point(p, Pos.Y + Size.Y / 2 - Components[i].Size.Y / 2);
                        p += Components[i].Size.X + Components[i].Margin.Right;
                    }
                }
                else if (VerticalAlignment == VerticalAlignment.Bottom)
                {
                    for (int i = 0; i < Components.Count; i++)
                    {
                        p += Components[i].Margin.Left;
                        Components[i].Pos = new Point(p, Pos.Y + Size.Y - Components[i].Size.Y - Components[i].Margin.Bottom);
                        p += Components[i].Size.X + Components[i].Margin.Right;
                    }
                }
            }
            else
            {
                if (VerticalAlignment == VerticalAlignment.Top)
                    p = Pos.Y;
                else if (VerticalAlignment == VerticalAlignment.Center)
                    p = Pos.Y + Size.Y / 2 - allSize.Y / 2;
                else if (VerticalAlignment == VerticalAlignment.Bottom)
                    p = Pos.Y + Size.Y - allSize.Y;

                if (HorizontalAlignment == HorizontalAlignment.Left)
                {
                    for (int i = 0; i < Components.Count; i++)
                    {
                        p += Components[i].Margin.Top;
                        Components[i].Pos = new Point(Pos.X + Components[i].Margin.Left, p);
                        p += Components[i].Size.Y + Components[i].Margin.Bottom;
                    }
                }
                else if (HorizontalAlignment == HorizontalAlignment.Center)
                {
                    for (int i = 0; i < Components.Count; i++)
                    {
                        p += Components[i].Margin.Top;
                        Components[i].Pos = new Point(Pos.X + Size.X / 2 - Components[i].Size.X / 2, p);
                        p += Components[i].Size.Y + Components[i].Margin.Bottom;
                    }
                }
                else if (HorizontalAlignment == HorizontalAlignment.Right)
                {
                    for (int i = 0; i < Components.Count; i++)
                    {
                        p += Components[i].Margin.Top;
                        Components[i].Pos = new Point(Pos.X + Size.X - Components[i].Size.X - Components[i].Margin.Right, p);
                        p += Components[i].Size.Y + Components[i].Margin.Bottom;
                    }
                }
            }
        }
    }
}
