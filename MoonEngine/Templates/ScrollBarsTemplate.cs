﻿using Microsoft.Xna.Framework;
using MoonEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Templates
{
    public class ScrollBarsTemplate
    {
        #region Свойства
        public Point Pos { get; set; } = Point.Zero; // позиция рабочей области
        public Point Size { get; set; } = new Point(1, 1); //размер рабочей области
        public ScrollBar ScrollBarHorizontal { get; set; } = null;
        public ScrollBar ScrollBarVertical { get; set; } = null;
        public Component Stub { get; set; } = null;
        #endregion

        public void CalcSize()
        {
            // Установить размеры скроллбарам
            // Если заглушка отображается или отображаются оба скроллбара
            if (Stub.IsVisible || (ScrollBarHorizontal.IsVisible && ScrollBarVertical.IsVisible))
            {
                ScrollBarVertical.Size = new Point(ScrollBarVertical.Size.X, Size.Y - ScrollBarHorizontal.Size.Y);
                ScrollBarHorizontal.Size = new Point(Size.X - ScrollBarVertical.Size.X, ScrollBarHorizontal.Size.Y);
            }
            // Если заглушка скрыта (и отображается один скроллбар или не отображается вообще)
            else
            {
                ScrollBarVertical.Size = new Point(ScrollBarVertical.Size.X, Size.Y);
                ScrollBarHorizontal.Size = new Point(Size.X, ScrollBarHorizontal.Size.Y);
            }

            // Установить размер заглушке
            Stub.Size = new Point(ScrollBarVertical.Size.X, ScrollBarHorizontal.Size.Y);
        }
        public void CalcPos()
        {
            // Установить позиции скроллбарам
            ScrollBarVertical.Pos = new Point(Pos.X + Size.X - ScrollBarVertical.Size.X, Pos.Y);
            ScrollBarHorizontal.Pos = new Point(Pos.X, Pos.Y + Size.Y - ScrollBarHorizontal.Size.Y);

            // Установить позицию заглушке
            Stub.Pos = new Point(Pos.X + Size.X - ScrollBarVertical.Size.X, Pos.Y + Size.Y - ScrollBarHorizontal.Size.Y);
        }
    }
}
