﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine
{
    public class Resources
    {
        public ContentManager ContentManager { get; }

        SpriteFont sp_system;
        Texture2D t2d_point;

        public Resources(ContentManager cm)
        { ContentManager = cm; }

        public void Load()
        {
            sp_system = ContentManager.Load<SpriteFont>(@"sf_system");
            t2d_point = ContentManager.Load<Texture2D>(@"t2d_point");
        }
    }
}
