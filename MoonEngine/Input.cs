﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MoonEngine
{
    public class Input
    {
        public KeyboardState KeyboardStateCurrent { get; private set; } = new KeyboardState();
        public KeyboardState KeyboardStateOld { get; private set; } = new KeyboardState();
        public MouseState MouseStateCurrent { get; private set; } = new MouseState();
        public MouseState MouseStateOld { get; private set; } = new MouseState();

        public void Update()
        {
            KeyboardStateOld = KeyboardStateCurrent; // обновляем предыдущее состояние клавиатуры
            MouseStateOld = MouseStateCurrent; // и мыши
            KeyboardStateCurrent = Keyboard.GetState(); // читаем новое
            MouseStateCurrent = Mouse.GetState();
        }

        // Клавиша нажата и отпущена
        public bool IsKeyDownUp(Keys key)
        {
            return (KeyboardStateOld.IsKeyDown(key) && KeyboardStateCurrent.IsKeyUp(key));
        }

        // Клавиша отпущена и нажата
        public bool IsKeyUpDown(Keys key)
        {
            return (KeyboardStateCurrent.IsKeyDown(key) && KeyboardStateOld.IsKeyUp(key));
        }

        // Клавиша нажата
        public bool IsKeyDown(Keys key)
        {
            return KeyboardStateCurrent.IsKeyDown(key);
        }

        // Мышь над прямоугольником
        public bool MouseOnRect(Vector2 pos, Vector2 size)
        {
            if ((MouseStateCurrent.X >= pos.X) && (MouseStateCurrent.X <= pos.X + size.X) && (MouseStateCurrent.Y >= pos.Y) && (MouseStateCurrent.Y <= pos.Y + size.Y))
                return true;
            else return false;
        }
        public bool MouseOnRect(Point pos, Point size)
        {
            if ((MouseStateCurrent.X >= pos.X) && (MouseStateCurrent.X <= pos.X + size.X) && (MouseStateCurrent.Y >= pos.Y) && (MouseStateCurrent.Y <= pos.Y + size.Y))
                return true;
            else return false;
        }
        public bool MouseOnRect(Rectangle rect)
        {
            if ((MouseStateCurrent.X >= rect.X) && (MouseStateCurrent.X <= rect.X + rect.Width) && (MouseStateCurrent.Y >= rect.Y) && (MouseStateCurrent.Y <= rect.Y + rect.Height))
                return true;
            else return false;
        }

        // Мышь была над прямоугольником
        public bool MouseOnRectOld(Point pos, Point size)
        {
            if ((MouseStateOld.X >= pos.X) && (MouseStateOld.X <= pos.X + size.X) && (MouseStateOld.Y >= pos.Y) && (MouseStateOld.Y <= pos.Y + size.Y))
                return true;
            else return false;
        }
        public bool MouseOnRectOld(Rectangle rect)
        {
            if ((MouseStateOld.X >= rect.X) && (MouseStateOld.X <= rect.X + rect.Width) && (MouseStateOld.Y >= rect.Y) && (MouseStateOld.Y <= rect.Y + rect.Height))
                return true;
            else return false;
        }

        // Перемещение мыши
        public Point MouseDeltaPos()
        {
            return MouseStateOld.Position - MouseStateCurrent.Position;
        }

        // Колёсико мыши
        public float MouseDeltaWheel()
        {
            return (MouseStateOld.ScrollWheelValue - MouseStateCurrent.ScrollWheelValue) / 120f;
        }

        // ЛКМ нажата и отпущена
        public bool IsMouseLeftDownUp()
        {
            return (MouseStateCurrent.LeftButton == ButtonState.Released && MouseStateOld.LeftButton == ButtonState.Pressed);
        }

        // ЛКМ отпущена и нажата
        public bool IsMouseLeftUpDown()
        {
            return (MouseStateCurrent.LeftButton == ButtonState.Pressed && MouseStateOld.LeftButton == ButtonState.Released);
        }

        // ЛКМ нажата
        public bool IsMouseLeftDown()
        {
            return (MouseStateCurrent.LeftButton == ButtonState.Pressed);
        }

        // ПКМ нажата и отпущена
        public bool IsMouseRightDownUp()
        {
            return (MouseStateCurrent.RightButton == ButtonState.Released && MouseStateOld.RightButton == ButtonState.Pressed);
        }

        // ПКМ отпущена и нажата
        public bool IsMouseRightUpDown()
        {
            return (MouseStateCurrent.RightButton == ButtonState.Pressed && MouseStateOld.RightButton == ButtonState.Released);
        }

        // ПКМ нажата
        public bool IsMouseRightDown()
        {
            return (MouseStateCurrent.RightButton == ButtonState.Pressed);
        }
    }
}
