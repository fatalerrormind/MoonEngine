﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine
{
    abstract public class Form : ContainerList
    {
        public Form(Core core) : base(core)
        {
            CanCatchMouse = true;
        }
        public override void Update()
        {
            if (IsEnabled)
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Update();
        }
        //public override void Layout()
        //{
        //    for (int i = 0; i < Components.Count; i++)
        //        Components[i].Layout();

        //    if (IsInvalidateLayout)
        //    {
        //        // Расчёт шаблона
        //        // ...
        //        IsInvalidateLayout = false;
        //        if (Core.DebugLogging)
        //            Core.DebugAddLog("Layout " + GetType().Name);
        //    }
        //}
        //virtual public void Render()
        //{
        //    if (IsInvalidateBuffer)
        //    {
        //        for (int i = 0; i < Components.Count; i++)
        //            Components[i].Render();

        //        PrepareBuffer();
        //        Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
        //        // Отрисовка под UI
        //        // ...
        //        for (int i = 0; i < Components.Count; i++)
        //            Components[i].Draw();
        //        // Отрисовка над UI
        //        // ...
        //        Core.Sb.End();

        //        IsInvalidateBuffer = false;
        //        if (Core.DebugLogging)
        //            Core.DebugAddLog("Render " + GetType().Name);
        //    }
        //}
    }
}
