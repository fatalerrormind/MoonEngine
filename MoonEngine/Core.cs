﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MoonEngine.Components;
using MoonEngine.Modules;
using MoonEngine;
using System;
using System.Collections.Generic;

namespace MoonEngine
{
    public class Core
    {
        public class CoreForms
        {
            List<Form> forms = new List<Form>();
            /// <summary>
            /// Получить количество форм в листе обработки
            /// </summary>
            public int Count
            {
                get { return forms.Count; }
            }
            /// <summary>
            /// Получить форму из листа
            /// </summary>
            public Form this[int i]
            {
                get { return forms[i]; }
                set { forms[i] = value; }
            }

            /// <summary>
            /// Добавить форму в лист обработки
            /// </summary>
            public void Add(Form form)
            {
                forms.Add(form);
            }
            /// <summary>
            /// Удалить форму из списка
            /// </summary>
            /// <param name="dispose">Нужно ли вызвать Dispose() перед удалением из списка</param>
            public void Remove(Form form, bool dispose)
            {
                for (int i = 0; i < forms.Count; i++)
                    if (forms[i] == form)
                    {
                        if (dispose)
                            forms[i].Dispose();
                        forms.RemoveAt(i);
                        break;
                    }
            }
            /// <summary>
            /// Удалить форму из списка
            /// </summary>
            /// <param name="dispose">Нужно ли вызвать Dispose() перед удалением из списка</param>
            public void Remove(int index, bool dispose)
            {
                if (dispose)
                    forms[index].Dispose();
                forms.RemoveAt(index);
            }

            /// <summary>
            /// Вызов проверки коллизии с курсором
            /// </summary>
            public void TryCatchMouse()
            {
                for (int i = forms.Count - 1; i >= 0; i--)
                    forms[i].TryCatchMouse(Point.Zero);
            }
            /// <summary>
            /// Просьба перерисовать буфер
            /// </summary>
            public void InvalidateBuffer()
            {
                for (int i = 0; i < forms.Count; i++)
                    forms[i].InvalidateBuffer();
            }
            /// <summary>
            /// Просьба пересчитать макет
            /// </summary>
            public void InvalidateLayout()
            {
                for (int i = 0; i < forms.Count; i++)
                    forms[i].InvalidateLayout();
            }
            
            /// <summary>
            /// Переместить форму на передний план
            /// </summary>
            public void ToFront(Form form)
            {
                for (int i = 0; i < forms.Count; i++)
                    if (forms[i] == form)
                    {
                        forms.Insert(forms.Count, forms[i]);
                        forms.RemoveAt(i);
                        break;
                    }
            }
            /// <summary>
            /// Переместить форму на передний план
            /// </summary>
            public void ToFront(int index)
            {
                forms.Insert(forms.Count, forms[index]);
                forms.RemoveAt(index);
            }

            /// <summary>
            /// Переместить форму на задний план
            /// </summary>
            public void ToBack(Component component)
            {
                for (int i = 0; i < forms.Count; i++)
                    if (forms[i] == component)
                    {
                        forms.Insert(0, forms[i]);
                        forms.RemoveAt(i + 1);
                    }
            }
            /// <summary>
            /// Переместить форму на задний план
            /// </summary>
            public void ToBack(int index)
            {
                forms.Insert(0, forms[index]);
                forms.RemoveAt(index + 1);
            }
        }

        public const string TestString = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdifghijklmnopqrstuvwxyz1234567890!?,.-+=()[]{}\/";
        public const int DefLogLines = 30;

        #region Свойства
        public CoreForms Forms { get; } = new CoreForms();
        public Input Input { get; private set; }
        public Control Control { get; private set; }
        public Settings Settings { get; private set; }
        public Game Game { get; private set; }
        public GraphicsDeviceManager Gdm { get; private set; }
        public GraphicsDevice Gd { get; private set; }
        public SpriteBatchShape Sb { get; private set; }
        public Point ScreenSize { get { return new Point(Gdm.PreferredBackBufferWidth, Gdm.PreferredBackBufferHeight); } }
        public int ScreenWidth { get { return Gdm.PreferredBackBufferWidth; } }
        public int ScreenHeight { get { return Gdm.PreferredBackBufferHeight; } }

        public float DtUpdate { get; private set; } = 0f;
        public float Ups { get; private set; } = 0f;

        public float DtDraw { get; private set; } = 0f;
        public float FramePerSecond { get; private set; } = 0f;

        public bool IsPaused { get; set; } = false;
        public bool DebugLogging { get; set; } = true;

        SpriteFont debugFont;
        public SpriteFont DebugFont
        {
            get { return debugFont; }
            set
            {
                if (debugFont != value && value != null)
                {
                    debugFont = value;
                    debugFontHeight = debugFont.MeasureString(TestString).Y;
                }
            }
        }
        
        public Color DebugTextBackgroundColor { get; set; } = Color.FromNonPremultiplied(0, 0, 0, 80);
        
        public bool DebugIsVisible { get; set; } = true;
        #endregion

        float debugFontHeight = 0f;
        int upsTotalUpdates = 0;
        float upsTotalTime = 0f;
        int fpsTotalFrames = 0;
        float fpsTotalTime = 0f;
        string debugDynamic = "";
        string[] debugLog = new string[DefLogLines];
        Vector2 debugDynamicPos = new Vector2(3f, 3f);

        // Ресурсы
        public SpriteFont sf_system;
        //public Texture2D t2d_cursor_normal;
        public Texture2D t2d_white_pixel;

        // Добавить запись в динамическую строку
        public void AddDynamicString(string str)
        {
            debugDynamic += $"\n{str}";
        }

        // Добавить запись в лог
        public void AddLogString(string str)
        {
            for (int i = 1; i < DefLogLines; i++)
                debugLog[i - 1] = debugLog[i];
            debugLog[DefLogLines - 1] = DateTime.Now.ToString("[HH:mm:ss.ff] ") + str.Split('\n')[0];
        }

        public Core(Game game, GraphicsDeviceManager gdm)
        {
            Game = game;
            Gdm = gdm;
            Gd = Game.GraphicsDevice;

            // Загрузка базовых ресурсов
            sf_system = Game.Content.Load<SpriteFont>(@"sf_system");
            t2d_white_pixel = Game.Content.Load<Texture2D>(@"t2d_white_pixel");
            //t2d_cursor_normal = Game.Content.Load<Texture2D>(@"t2d_cursor_normal");
            DebugFont = sf_system;
            AddLogString("MoonEngine: Resources loaded");

            // Инициализация объектов
            Input = new Input();
            Control = new Control(this);
            Sb = new SpriteBatchShape(Game.GraphicsDevice, t2d_white_pixel);
            Settings = new Settings(this);

            for (int i = 0; i < DefLogLines; i++)
                debugLog[i] = "";

            AddLogString("MoonEngine: Initialized");
        }
        
        public void Update(GameTime gameTime)
        {
            DtUpdate = (float)gameTime.ElapsedGameTime.TotalSeconds; // обновляем дельтатайм

            if (DebugIsVisible)
            {
                // Вычисляем UPS
                upsTotalUpdates++;
                upsTotalTime += DtUpdate;
                if (upsTotalTime >= 0.5f)
                {
                    Ups = upsTotalUpdates / upsTotalTime;
                    upsTotalUpdates = 0;
                    upsTotalTime = 0f;
                }
            }

            Input.Update(); // обновляем аппаратный ввод
            Control.Update(); // обновляем ввод компонентов

            // Обработка клавиш
            if (Input.IsKeyDownUp(Keys.F1))
                DebugIsVisible = !DebugIsVisible;
            if (Input.IsKeyDownUp(Keys.F2))
                IsPaused = !IsPaused;
            if (Input.IsKeyDownUp(Keys.F12))
                AddLogString("Screenshot saved");

            // Обновление компонентов
            if (!IsPaused)
                for (int i = 0; i < Forms.Count; i++)
                    Forms[i].Update();
        }
        public void Draw(GameTime gameTime)
        {
            DtDraw = (float)gameTime.ElapsedGameTime.TotalSeconds; // обновляем дельтатайм

            if (DebugIsVisible)
            {
                // Вычисляем FPS
                fpsTotalFrames++;
                fpsTotalTime += DtDraw;
                if (fpsTotalTime >= 0.5f)
                {
                    FramePerSecond = fpsTotalFrames / fpsTotalTime;
                    fpsTotalFrames = 0;
                    fpsTotalTime = 0f;
                }
                debugDynamic =
                    $"FPS: {FramePerSecond.ToString("F1")}   UPS: {Ups.ToString("F1")}   TargetElapsedTime: {Game.TargetElapsedTime.Ticks}   Resolution: {ScreenWidth}x{ScreenHeight}   DebugLogging: {DebugLogging}";
            }

            for (int i = 0; i < Forms.Count; i++)
                Forms[i].Layout();
            for (int i = 0; i < Forms.Count; i++)
                Forms[i].Render();
            
            Gd.SetRenderTarget(null);

            Gd.Clear(Color.Black);
            Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
            // Рисуем формы
            for (int i = 0; i < Forms.Count; i++)
                Forms[i].Draw();

            // Рисуем дебагинфо
            if (DebugIsVisible)
            {
                string[] strs = debugDynamic.Split('\n');
                for (int i = 0; i < strs.Length; i++)
                {
                    Vector2 strSize = debugFont.MeasureString(strs[i]);
                    Sb.DrawRectangleFill((int)debugDynamicPos.X, (int)(debugDynamicPos.Y + i * strSize.Y), (int)strSize.X, (int)strSize.Y, DebugTextBackgroundColor);
                    Sb.DrawString(debugFont, strs[i], new Vector2(debugDynamicPos.X, debugDynamicPos.Y + i * strSize.Y), Color.White);
                }

                float LogY = Gdm.PreferredBackBufferHeight - (int)debugFontHeight * DefLogLines - debugDynamicPos.Y;

                for (int i = 0; i < DefLogLines; i++)
                {
                    Vector2 strSize = debugFont.MeasureString(debugLog[i]);
                    Sb.DrawRectangleFill((int)debugDynamicPos.X, (int)(LogY + i * strSize.Y), (int)strSize.X, (int)strSize.Y, DebugTextBackgroundColor);
                    Sb.DrawString(debugFont, debugLog[i], new Vector2(debugDynamicPos.X, LogY + i * strSize.Y), Color.White);
                }
            }
            //Рисуем курсор
            //Sb.Draw(t2d_cursor_normal, Control.MouseStateCurrent.Position.ToVector2(), Color.White);
            Sb.End();
        }
    }
}
