﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine
{
    static public class RandomHelper
    {
        static public Random Random { get; set; } = new Random();
        
        /// <summary>
        /// Возвращает true или false с равной вероятностью
        /// </summary>
        static public bool Boolean()
        {
            return Random.Next(0, 2) == 0;
        }

        /// <summary>
        /// Возвращает неотрицательное случайное целое число
        /// </summary>
        static public int Int()
        {
            return Random.Next();
        }
        /// <summary>
        /// Возвращает случайное целое число в указанном диапазоне
        /// </summary>
        /// <param name="min">Нижняя граница включительно</param>
        /// <param name="max">Верхняя граница включительно</param>
        static public int Int(int min, int max)
        {
            return Random.Next(min, max + 1);
        }

        /// <summary>
        /// Возвращает случайное число с плавающей запятой, которое больше или равно 0.0f и меньше 1.0f
        /// </summary>
        static public float Float()
        {
            return (float)Random.NextDouble();
        }
        /// <summary>
        /// Возвращает случайное число с плавающей запятой в указанном диапазоне
        /// </summary>
        /// <param name="min">Нижняя граница</param>
        /// <param name="max">Верхняя граница</param>
        static public float Float(float min, float max)
        {
            return (float)(Random.NextDouble() * (max - min) + min);
        }

        /// <summary>
        /// Возвращает случайный цвет RGBA, где RGB случайны, а A=255
        /// </summary>
        static public Color ColorRGB()
        {
            return new Color(Random.Next(0, 256), Random.Next(0, 256), Random.Next(0, 256), 255);
        }
        /// <summary>
        /// Возвращает случайный цвет RGBA, где RGBA
        /// </summary>
        static public Color ColorRGBA()
        {
            return new Color(Random.Next(0, 256), Random.Next(0, 256), Random.Next(0, 256), Random.Next(0, 256));
        }

    }
}
