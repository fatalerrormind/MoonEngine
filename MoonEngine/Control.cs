﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MoonEngine.Components;

namespace MoonEngine
{
    public class Control
    {
        public Core Core { get; }

        public bool MouseCatch { get; set; } = false;
        public Component OnMouseOld { get; set; } = null;
        public Component OnMouse { get; set; } = null;
        public Component MouseEnter { get; set; } = null;
        public Component MouseOut { get; set; } = null;

        public Component LeftDownOld { get; set; } = null;
        public Component LeftDown { get; set; } = null;
        public Component LeftUpDown { get; set; } = null;
        public Component LeftDownUp { get; set; } = null;
        public Component LeftClicked { get; set; } = null;

        public Component RightDownOld { get; set; } = null;
        public Component RightDown { get; set; } = null;
        public Component RightUpDown { get; set; } = null;
        public Component RightDownUp { get; set; } = null;
        public Component RightClicked { get; set; } = null;

        public Control(Core core)
        {
            Core = core;
        }

        public void Update()
        {
            OnMouseOld = OnMouse; // обновляем предыдущее состояние компонентов
            LeftDownOld = LeftDown;
            RightDownOld = RightDown;

            // Сброс флагов нажатия и курсора над объектом
            if (!Core.Input.IsMouseLeftDown())
                LeftDown = null;
            if (!Core.Input.IsMouseRightDown())
                RightDown = null;
            if (LeftDown == null && RightDown == null)
            {
                MouseCatch = false;
                OnMouse = null;
            }

            Core.Forms.TryCatchMouse(); // вызов опроса компонентов

            // Установка IsLeftDown
            if (Core.Input.IsMouseLeftUpDown())
                LeftDown = OnMouse;

            // Установка IsRightDown
            if (Core.Input.IsMouseRightUpDown())
                RightDown = OnMouse;

            // Сброс/установка флагов для событий OnMouseEnter и OnMouseOut
            if (OnMouse != null && OnMouse != OnMouseOld)
            {
                MouseEnter = OnMouse;
                if (MouseEnter.IsVisualReactionOnMouse)
                    MouseEnter.InvalidateBuffer();
            }
            else MouseEnter = null;
            if (OnMouseOld != null && OnMouse != OnMouseOld)
            {
                MouseOut = OnMouseOld;
                if (MouseOut.IsVisualReactionOnMouse)
                    MouseOut.InvalidateBuffer();
            } 
            else MouseOut = null;

            // Сброс/установка флагов для событий IsLeftUpDown и IsLeftDownUp
            if (LeftDown != null && LeftDown != LeftDownOld) // установка UpDown
            {
                LeftUpDown = LeftDown;
                if (LeftUpDown.IsVisualReactionIsLeftDown)
                    LeftUpDown.InvalidateBuffer();
            }
            else LeftUpDown = null; // сброс UpDown
            if (LeftDownOld != null && LeftDown != LeftDownOld) // установка DownUp
            {
                LeftDownUp = LeftDownOld;
                if (LeftDownOld.IsVisualReactionIsLeftDown)
                    LeftDownOld.InvalidateBuffer();
            }
            else LeftDownUp = null; // сброс DownUp
            if (LeftDownOld != null && LeftDown != LeftDownOld && OnMouse == LeftDownOld && RightDown == null) // установка Clicked
                LeftClicked = LeftDownOld;
            else LeftClicked = null; // сброс Clicked

            // Сброс/установка флагов для событий IsRightUpDown и IsRightDownUp
            if (RightDown != null && RightDown != RightDownOld)
            {
                RightUpDown = RightDown;
                if (RightUpDown.IsVisualReactionIsRightDown)
                    RightUpDown.InvalidateBuffer();
            }
            else RightUpDown = null;
            if (RightDownOld != null && RightDown != RightDownOld)
            {
                RightDownUp = RightDownOld;
                if (RightDownOld.IsVisualReactionIsRightDown)
                    RightDownOld.InvalidateBuffer();
            }
            else RightDownUp = null;
            if (RightDownOld != null && RightDown != RightDownOld && OnMouse == RightDownOld && LeftDown == null)
                RightClicked = RightDownOld;
            else RightClicked = null;
        }
    }
}
