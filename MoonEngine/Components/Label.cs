﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MoonEngine.Components
{
    public class Label : Component
    {
        #region Стиль
        public class LabelStyle : ComponentStyle
        {
            public SpriteFont Font { get; set; }
            public Color FontColor { get; set; }
            public string Text { get; set; }

            public LabelStyle(Core core) : base(core)
            {
                Font = core.sf_system;
                FontColor = Color.Black;
                Text = "label";
            }
            public LabelStyle(Label component) : base(component)
            {
                Font = component.Font;
                FontColor = component.FontColor;
                Text = component.Text;
            }
        }
        
        public void SetStyle(LabelStyle style)
        {
            base.SetStyle(style);
            Font = style.Font;
            FontColor = style.FontColor;
            Text = style.Text;
        }
        #endregion
        #region Свойства
        SpriteFont font;
        public SpriteFont Font
        {
            get { return font; }
            set { if (font != value) { font = value; InvalidateLayout(); InvalidateBuffer(); } }
        }

        Color fontColor;
        public Color FontColor
        {
            get { return fontColor; }
            set { if (fontColor != value) { fontColor = value; InvalidateBuffer(); } }
        }

        string text;
        public string Text
        {
            get { return text; }
            set { if (text != value) { text = value; InvalidateLayout(); InvalidateBuffer(); } }
        }
        #endregion

        public Label(Core core, string text, LabelStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new LabelStyle(Core));
            else
                SetStyle(style);

            Text = text;
        }
        public Label(Core core, LabelStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new LabelStyle(Core));
            else
                SetStyle(style);
        }

        public override void Layout()
        {
            if (IsInvalidateLayout)
            {
                Size = Font.MeasureString(text).ToPoint();
                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }

        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                Core.Sb.DrawString(Font, text, Vector2.Zero, FontColor);
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
