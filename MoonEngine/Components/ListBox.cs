﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Components
{
    public class ListBox : ContainerList
    {
        #region Стиль
        #endregion
        #region Свойства
        /// <summary>
        /// Полоса прокрутки
        /// </summary>
        public ScrollBar ScrollBarVertical { get; }

        /// <summary>
        /// Высота ячейки
        /// </summary>
        public int CellHeight { get; set; } = 20;

        /// <summary>
        /// Выравнивание компонента внутри ячейки по горизонтали
        /// </summary>
        public HorizontalAlignment HorizontalAlignment
        {
            get { return gridTemplate.HorizontalAlignment; }
            set
            {
                if (gridTemplate.HorizontalAlignment != value)
                {
                    gridTemplate.HorizontalAlignment = value;
                    InvalidateLayout();
                }
            }
        }

        /// <summary>
        /// Выравнивание компонента внутри ячейки по вертикали
        /// </summary>
        public VerticalAlignment VerticalAlignment
        {
            get { return gridTemplate.VerticalAlignment; }
            set
            {
                if (gridTemplate.VerticalAlignment != value)
                {
                    gridTemplate.VerticalAlignment = value;
                    InvalidateLayout();
                }
            }
        }

        /// <summary>
        /// Флаг определяет, у каких компонентов и списка будет вызваться Update()
        /// False - только у тех, что находятся в видимой часте ListBox'а
        /// True - у всех
        /// </summary>
        public bool AllowUpdateForAllComponents { get; set; } = false;

        /// <summary>
        /// Индекс первого компонента в видимой части
        /// Следует учитывать, что значения, находящиеся вне границ листа компонентов, округляются до ближайшего значения из диапазона
        /// Если лист компонентов пустой, то свойство равно 0 (нулю)
        /// </summary>
        public int FirstVisibleIndex { get; private set; } = 0;

        /// <summary>
        /// Индекс последнего компонента в видимой части
        /// Следует учитывать, что значения, находящиеся вне границ листа компонентов, округляются до ближайшего значения из диапазона
        /// Если лист компонентов пустой, то свойство равно 0 (нулю)
        /// </summary>
        public int LastVisibleIndex { get; private set; } = 0;

        SelectMode selectMode = SelectMode.Single; // !!! ПОПРАВЬ ПОДСКАЗКУ ЕСЛИ СДЕЛАЕШЬ Multiple, возможно стоит сделать коррекцию свойств при изменении режима
        /// <summary>
        /// Режим выбора компонента.
        /// None - при нажатии не происходит ни сброса предыдущего выделения, ни нового выделения
        /// Single - при нажатии происходит замена предыдущего выделения новым
        /// Multiple - при нажатии происходит смена состояния ячейки с невыделенной в выделенную и наоборот (режим не реализован)
        /// </summary>
        public SelectMode SelectMode
        {
            get { return selectMode; }
            set
            {
                if (selectMode != value)
                {
                    selectMode = value;
                    // Здесь нужно откорректировать уже имеющееся выделение
                    //InvalidateBuffer();
                }
            }
        }

        int selectedIndex = -1;
        /// <summary>
        /// Индекс выбранного компонента. Если не выбран то SelectedIndex = -1
        /// </summary>
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                if (value < -1 || value >= Components.Count)
                    value = -1;
                if (selectedIndex != value)
                {
                    selectedIndex = value;
                    InvalidateBuffer();
                }
            }
        }
        
        Color selectionColor;
        /// <summary>
        /// Цвет тонировки текстуры выделения выбранного компонента
        /// </summary>
        public Color SelectionColor
        {
            get { return selectionColor; }
            set { if (selectionColor != value) { selectionColor = value; InvalidateBuffer(); } }
        }

        Texture2D selectionTexture;
        /// <summary>
        /// Текстура выделения выбранного компонента
        /// </summary>
        public Texture2D SelectionTexture
        {
            get { return selectionTexture; }
            set { if (selectionTexture != value) { selectionTexture = value; InvalidateBuffer(); } }
        }
        #endregion

        GridTemplate gridTemplate;
        int indexCatchClick = 0;

        public ListBox(Core core) : base(core)
        {
            CanCatchMouse = true;
            IsVisualReactionIsLeftDown = true;
            ScrollBarVertical = new ScrollBar(Core);
            ScrollBarVertical.Owner = this;
            ScrollBarVertical.Orientation = Orientation.Vertical;

            gridTemplate = new GridTemplate();
            gridTemplate.Pos = Point.Zero;
            gridTemplate.Orientation = Orientation.Vertical;
            gridTemplate.HorizontalAlignment = HorizontalAlignment.Left;
            gridTemplate.VerticalAlignment = VerticalAlignment.Center;
            gridTemplate.GridSize = 1;
            //gridTemplate.CellSize = new Point(50, 10);
            gridTemplate.Components = Components;
        }

        public override void Dispose()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Dispose();

            ScrollBarVertical.Dispose();

            if (Buffer != null)
                Buffer.Dispose();

            if (Core.DebugLogging)
                Core.AddLogString("Dispose " + GetType().Name);
        }

        public override void TryCatchMouseInternal(Point ownerPos)
        {
            ScrollBarVertical.TryCatchMouse(ownerPos);

            if (Components.Count > 0)
                for (int i = LastVisibleIndex; i >= FirstVisibleIndex; i--)
                    Components[i].TryCatchMouse(ownerPos);
        }

        public override void Update()
        {
            if (IsEnabled && IsVisible)
            {
                if (AllowUpdateForAllComponents)
                    for (int i = 0; i < Components.Count; i++)
                        Components[i].Update();
                else if (Components.Count > 0)
                    for (int i = FirstVisibleIndex; i <= LastVisibleIndex; i++)
                        Components[i].Update();

                ScrollBarVertical.Update();

                if (SelectMode != SelectMode.None)
                {
                    if (this == Core.Control.LeftUpDown)
                    {
                        indexCatchClick = (GetMousePosOnComponent().Y + (int)ScrollBarVertical.DataCurrent) / CellHeight;
                    }
                    else if (this == Core.Control.LeftDownUp)
                    {
                        int i = (GetMousePosOnComponent().Y + (int)ScrollBarVertical.DataCurrent) / CellHeight;

                        if (indexCatchClick == i && i >= 0 && i < Components.Count)
                        {
                            if (SelectMode == SelectMode.Single)
                                SelectedIndex = i;
                            else if(SelectMode == SelectMode.Multiple)
                                SelectedIndex = i; // заглушка реализации мультивыделения
                        }
                    }
                }
            }
        }
        public override void Layout()
        {
            if (IsInvalidateLayout)
            {
                // Устанавливаем данные скроллбару
                ScrollBarVertical.DataSize = Components.Count * CellHeight;
                ScrollBarVertical.ViewSize = Size.Y;
                ScrollBarVertical.Size = new Point(ScrollBarVertical.Size.X, Size.Y);

                // Считаем индексы видимых компонентов
                FirstVisibleIndex = (int)ScrollBarVertical.DataCurrent / CellHeight;
                LastVisibleIndex = ((int)ScrollBarVertical.DataCurrent + (int)ScrollBarVertical.ViewSize) / CellHeight;

                // Корректируем индексы видимых компонентов
                if (FirstVisibleIndex < 0) FirstVisibleIndex = 0;
                else if (FirstVisibleIndex >= Components.Count) FirstVisibleIndex = Components.Count - 1;
                if (LastVisibleIndex < 0) LastVisibleIndex = 0;
                else if (LastVisibleIndex >= Components.Count) LastVisibleIndex = Components.Count - 1;
            }

            // Вызываем Layout() видимых компонентов
            if (Components.Count > 0)
                for (int i = FirstVisibleIndex; i <= LastVisibleIndex; i++)
                    Components[i].Layout();

            ScrollBarVertical.Layout();

            if (IsInvalidateLayout)
            {
                ScrollBarVertical.Pos = new Point(Size.X - ScrollBarVertical.Size.X, 0); // устанавливам позицию скроллбару
                gridTemplate.Pos = new Point(0, -(int)ScrollBarVertical.DataCurrent); // позицию шаблону грида

                // Размер ячейки грида
                if (ScrollBarVertical.IsVisible) 
                    gridTemplate.CellSize = new Point(Size.X - ScrollBarVertical.Size.X, CellHeight); 
                else
                    gridTemplate.CellSize = new Point(Size.X, CellHeight);

                gridTemplate.CalcPos(); // просчитываем позиции шаблоном

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                if (Components.Count > 0)
                    for (int i = FirstVisibleIndex; i <= LastVisibleIndex; i++)
                        Components[i].Render();

                ScrollBarVertical.Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
                
                if (Components.Count > 0)
                    for (int i = FirstVisibleIndex; i <= LastVisibleIndex; i++)
                    {
                        if (i == SelectedIndex)
                            Core.Sb.DrawRectangleFill(0, i * gridTemplate.CellSize.Y - (int)ScrollBarVertical.DataCurrent, gridTemplate.CellSize.X, gridTemplate.CellSize.Y, Color.Green);
                        if (i == indexCatchClick && Core.Control.LeftDown == this)
                            Core.Sb.DrawRectangleFill(0, i * gridTemplate.CellSize.Y - (int)ScrollBarVertical.DataCurrent, gridTemplate.CellSize.X, gridTemplate.CellSize.Y, Color.Red);
                        Components[i].Draw();
                    }

                ScrollBarVertical.Draw();
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
