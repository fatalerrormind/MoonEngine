﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace MoonEngine.Components
{
    public class StackPanel : ContainerList
    {
        #region Свойства
        bool autoSizeHorizontal = true;
        public bool AutoSizeHorizontal
        {
            get { return autoSizeHorizontal; }
            set { if (autoSizeHorizontal != value) { autoSizeHorizontal = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        bool autoSizeVertical = true;
        public bool AutoSizeVertical
        {
            get { return autoSizeVertical; }
            set { if (autoSizeVertical != value) { autoSizeVertical = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        Orientation orientation = Orientation.Horizontal;
        public Orientation Orientation
        {
            get { return orientation; }
            set
            {
                if (orientation != value)
                {
                    orientation = value;
                    InvalidateLayout();
                    InvalidateBuffer();
                }
            }
        }

        HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left;
        public HorizontalAlignment HorizontalAlignment
        {
            get { return horizontalAlignment; }
            set { if (horizontalAlignment != value) { horizontalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        VerticalAlignment verticalAlignment = VerticalAlignment.Top;
        public VerticalAlignment VerticalAlignment
        {
            get { return verticalAlignment; }
            set { if (verticalAlignment != value) { verticalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }
        #endregion

        public StackPanel(Core core) : base(core)
        { }

        public override void Layout()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Layout();

            if (IsInvalidateLayout)
            {
                Point allSize = new Point(0);
                Point maxSize = new Point(0);

                for (int i = 0; i < Components.Count; i++)
                {
                    Point curSize = new Point(
                        Components[i].Margin.Left + Components[i].Size.X + Components[i].Margin.Right,
                        Components[i].Margin.Top + Components[i].Size.Y + Components[i].Margin.Bottom);

                    allSize += curSize;
                    if (maxSize.X < curSize.X) maxSize.X = curSize.X;
                    if (maxSize.Y < curSize.Y) maxSize.Y = curSize.Y;
                }

                int pos = 0;

                if (orientation == Orientation.Horizontal)
                {
                    Size = new Point(autoSizeHorizontal ? allSize.X : Size.X, autoSizeVertical ? maxSize.Y : Size.Y);

                    if (horizontalAlignment == HorizontalAlignment.Left)
                        pos = 0;
                    else if (horizontalAlignment == HorizontalAlignment.Center)
                        pos = Size.X / 2 - allSize.X / 2;
                    else if (horizontalAlignment == HorizontalAlignment.Right)
                        pos = Size.X - allSize.X;

                    if (verticalAlignment == VerticalAlignment.Top)
                    {
                        for (int i = 0; i < Components.Count; i++)
                        {
                            pos += Components[i].Margin.Left;
                            Components[i].Pos = new Point(pos, Components[i].Margin.Top);
                            pos += Components[i].Size.X + Components[i].Margin.Right;
                        }
                    }
                    else if (verticalAlignment == VerticalAlignment.Center)
                    {
                        for (int i = 0; i < Components.Count; i++)
                        {
                            pos += Components[i].Margin.Left;
                            Components[i].Pos = new Point(pos, Size.Y / 2 - Components[i].Size.Y / 2);
                            pos += Components[i].Size.X + Components[i].Margin.Right;
                        }
                    }
                    else if (verticalAlignment == VerticalAlignment.Bottom)
                    {
                        for (int i = 0; i < Components.Count; i++)
                        {
                            pos += Components[i].Margin.Left;
                            Components[i].Pos = new Point(pos, Size.Y - Components[i].Size.Y - Components[i].Margin.Bottom);
                            pos += Components[i].Size.X + Components[i].Margin.Right;
                        }
                    }
                }
                else
                {
                    Size = new Point(autoSizeHorizontal ? maxSize.X : Size.X, autoSizeVertical ? allSize.Y : Size.Y);

                    if (verticalAlignment == VerticalAlignment.Top)
                        pos = 0;
                    else if (verticalAlignment == VerticalAlignment.Center)
                        pos = Size.Y / 2 - allSize.Y / 2;
                    else if (verticalAlignment == VerticalAlignment.Bottom)
                        pos = Size.Y - allSize.Y;

                    if (horizontalAlignment == HorizontalAlignment.Left)
                    {
                        for (int i = 0; i < Components.Count; i++)
                        {
                            pos += Components[i].Margin.Top;
                            Components[i].Pos = new Point(Components[i].Margin.Left, pos);
                            pos += Components[i].Size.Y + Components[i].Margin.Bottom;
                        }
                    }
                    else if (horizontalAlignment == HorizontalAlignment.Center)
                    {
                        for (int i = 0; i < Components.Count; i++)
                        {
                            pos += Components[i].Margin.Top;
                            Components[i].Pos = new Point(Size.X / 2 - Components[i].Size.X / 2, pos);
                            pos += Components[i].Size.Y + Components[i].Margin.Bottom;
                        }
                    }
                    else if (horizontalAlignment == HorizontalAlignment.Right)
                    {
                        for (int i = 0; i < Components.Count; i++)
                        {
                            pos += Components[i].Margin.Top;
                            Components[i].Pos = new Point(Size.X - Components[i].Size.X - Components[i].Margin.Right, pos);
                            pos += Components[i].Size.Y + Components[i].Margin.Bottom;
                        }
                    }
                }


                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
    }
}
