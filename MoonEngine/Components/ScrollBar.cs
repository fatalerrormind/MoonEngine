﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoonEngine.Modules;

namespace MoonEngine.Components
{
    public class ScrollBar : Component
    {
        #region Стиль
        public class ScrollBarStyle : ComponentStyle
        {
            public Button.ButtonStyle ButtonMinusStyle { get; set; }
            public Button.ButtonStyle ButtonPlusStyle { get; set; }
            public Button.ButtonStyle ButtonThumbStyle { get; set; }
            public Orientation Orientation { get; set; }
            public float Step { get; set; }
            public bool AutoIsEnabled { get; set; }
            public Color BarColor { get; set; }
            public Texture2D BarTexture { get; set; }
            public float RepeatInterval { get; set; }


            public ScrollBarStyle(Core core) : base(core)
            {
                IsClickable = true;
                Size = new Point(10);

                ButtonMinusStyle = new Button.ButtonStyle(core);
                ButtonPlusStyle = new Button.ButtonStyle(core);
                ButtonThumbStyle = new Button.ButtonStyle(core);
                Orientation = Orientation.Horizontal;
                Step = 10;
                AutoIsEnabled = true;
                BarColor = Color.DimGray;
                BarTexture = core.t2d_white_pixel;
                RepeatInterval = 0.1f;
            }
            public ScrollBarStyle(ScrollBar component) : base(component)
            {
                ButtonMinusStyle = new Button.ButtonStyle(component.ButtonMinus);
                ButtonPlusStyle = new Button.ButtonStyle(component.ButtonPlus);
                ButtonThumbStyle = new Button.ButtonStyle(component.ButtonThumb);
                Orientation = component.Orientation;
                Step = component.Step;
                AutoIsEnabled = component.AutoIsEnabled;
                BarColor = component.BarColor;
                BarTexture = component.BarTexture;
                RepeatInterval = component.RepeatInterval;
            }
        }
        public void SetStyle(ScrollBarStyle style)
        {
            base.SetStyle(style);
            ButtonMinus.SetStyle(style.ButtonMinusStyle);
            ButtonPlus.SetStyle(style.ButtonPlusStyle);
            ButtonThumb.SetStyle(style.ButtonThumbStyle);
            Orientation = style.Orientation;
            Step = style.Step;
            AutoIsEnabled = style.AutoIsEnabled;
            BarColor = style.BarColor;
            BarTexture = style.BarTexture;
            RepeatInterval = style.RepeatInterval;
        }
        #endregion
        #region Свойства
        float dataSize = 10;
        public float DataSize
        {
            get { return dataSize; }
            set
            {
                if (value < 0f) // коррекция нового DataSize
                    value = 0f;

                if (dataSize != value)
                {
                    dataSize = value;
                    correctDataCurrent(); // коррекция DataCurrent
                    correctIsEnabled();
                    InvalidateLayout();
                    InvalidateBuffer();
                    if (Owner != null) Owner.InvalidateLayout();
                }
            }
        }

        float viewSize = 1f;
        public float ViewSize
        {
            get { return viewSize; }
            set
            {
                if (value < 0f)
                    value = 0f;

                if (viewSize != value)
                {
                    viewSize = value;
                    correctDataCurrent(); // коррекция DataCurrent
                    correctIsEnabled();
                    InvalidateLayout();
                    InvalidateBuffer();
                    if (Owner != null) Owner.InvalidateLayout();
                }
            }
        }

        float dataCurrent = 0f;
        public float DataCurrent
        {
            get { return dataCurrent; }
            set
            {
                if (value > dataSize - viewSize)
                    value = dataSize - viewSize;
                if (value < 0f)
                    value = 0f;

                if (dataCurrent != value)
                {
                    dataCurrent = value;
                    correctIsEnabled();
                    InvalidateLayout();
                    if (Owner != null) Owner.InvalidateLayout();
                }
            }
        }

        Orientation orientation;
        public Orientation Orientation
        {
            get { return orientation; }
            set
            {
                if (orientation != value)
                {
                    orientation = value;
                    InvalidateLayout();
                    InvalidateBuffer();
                    if (Owner != null) Owner.InvalidateLayout();
                }
            }
        }

        public float Step { get; set; }
        
        bool autoIsEnabled;
        public bool AutoIsEnabled
        {
            get { return autoIsEnabled; }
            set
            {
                if (autoIsEnabled != value)
                {
                    autoIsEnabled = value;
                    correctIsEnabled();
                    InvalidateLayout();
                }
            }
        }

        Color barColor;
        public Color BarColor
        {
            get { return barColor; }
            set { if (barColor != value) { barColor = value; InvalidateBuffer(); } }
        }

        Texture2D barTexture;
        public Texture2D BarTexture
        {
            get { return barTexture; }
            set { if (barTexture != value) { barTexture = value; InvalidateBuffer(); } }
        }

        public float RepeatInterval
        {
            get { return repeatTimer.Interval; }
            set { repeatTimer.Interval = value; }
        }

        public Button ButtonMinus { get; }
        public Button ButtonPlus { get; }
        public Button ButtonThumb { get; }
        #endregion

        Timer repeatTimer = new Timer();
        Point mouseCatchPos = new Point(0);
        int mouseCatchThumbPos = 0;

        // Конструктор
        public ScrollBar(Core core, ScrollBarStyle style = null) : base(core)
        {
            ButtonMinus = new Button(Core);
            ButtonPlus = new Button(Core);
            ButtonThumb = new Button(Core);

            ButtonMinus.Owner = this;
            ButtonPlus.Owner = this;
            ButtonThumb.Owner = this;

            if (style == null)
                SetStyle(new ScrollBarStyle(Core));
            else
                SetStyle(style);
        }

        #region Вспомогательные методы
        // Корректирует состояние IsEnabled и IsVisible
        void correctIsEnabled()
        {
            if (AutoIsEnabled)
                IsEnabled = DataSize > ViewSize;
        }
        // Проверить и скорректировать DataCurrent
        void correctDataCurrent()
        {
            if (dataCurrent > dataSize - viewSize)
                dataCurrent = dataSize - viewSize;
            if (dataCurrent < 0f)
                dataCurrent = 0f;

        }
        // Ресчитать и изменить DataCurrent
        void calcDataCurrent()
        {
            if (orientation == Orientation.Horizontal)
                DataCurrent = (ButtonThumb.Pos.X - ButtonMinus.Size.X) * (dataSize - viewSize) / (Size.X - ButtonMinus.Size.X - ButtonPlus.Size.X - ButtonThumb.Size.X);
            else
                DataCurrent = (ButtonThumb.Pos.Y - ButtonMinus.Size.Y) * (dataSize - viewSize) / (Size.Y - ButtonMinus.Size.Y - ButtonPlus.Size.Y - ButtonThumb.Size.Y);
        }
        // Ресчитать и изменить размер ползунка
        void calcThumbSize()
        {
            int size;

            if (orientation == Orientation.Horizontal)
            {
                if (dataSize <= 0)
                    size = Size.X - ButtonMinus.Size.X - ButtonPlus.Size.X;

                else
                {
                    size = (int)((Size.X - ButtonMinus.Size.X - ButtonPlus.Size.X) / (dataSize / viewSize));

                    if (size > Size.X - ButtonMinus.Size.X - ButtonPlus.Size.X)
                        size = Size.X - ButtonMinus.Size.X - ButtonPlus.Size.X;
                    else if (size < Size.Y)
                        size = Size.Y;
                     
                }

                ButtonThumb.Size = new Point(size, ButtonThumb.Size.Y);
            }
            else
            {
                if (dataSize <= 0)
                    size = Size.Y - ButtonMinus.Size.Y - ButtonPlus.Size.Y;
                else
                {
                    size = (int)((Size.Y - ButtonMinus.Size.Y - ButtonPlus.Size.Y) / (dataSize / viewSize));

                    if (size > Size.Y - ButtonMinus.Size.Y - ButtonPlus.Size.Y)
                        size = Size.Y - ButtonMinus.Size.Y - ButtonPlus.Size.Y;
                    else if (size < Size.X)
                        size = Size.X;
                }

                ButtonThumb.Size = new Point(ButtonThumb.Size.X, size);
            }
        }
        // Установить позицию ползунка
        void setThumbPos(int pos)
        {
            if (orientation == Orientation.Horizontal)
            {
                if (pos < ButtonMinus.Size.X)
                    pos = ButtonMinus.Size.X;
                else if (pos > Size.X - ButtonThumb.Size.X - ButtonPlus.Size.X)
                    pos = Size.X - ButtonThumb.Size.X - ButtonPlus.Size.X;

                ButtonThumb.Pos = new Point(pos, ButtonThumb.Pos.Y);
            }
            else
            {
                if (pos < ButtonMinus.Size.Y)
                    pos = ButtonMinus.Size.Y;
                else if (pos > Size.Y - ButtonThumb.Size.Y - ButtonPlus.Size.Y)
                    pos = Size.Y - ButtonThumb.Size.Y - ButtonPlus.Size.Y;

                ButtonThumb.Pos = new Point(ButtonThumb.Pos.X, pos);
            }
        }
        // Расчитать и вернуть позицию ползунка
        int getThumbPos()
        {
            if (orientation == Orientation.Horizontal)
                return (int)((Size.X - ButtonMinus.Size.X - ButtonPlus.Size.X - ButtonThumb.Size.X) * dataCurrent / (dataSize - viewSize)) + ButtonMinus.Size.X;
            else
                return (int)((Size.Y - ButtonMinus.Size.Y - ButtonPlus.Size.Y - ButtonThumb.Size.Y) * dataCurrent / (dataSize - viewSize)) + ButtonMinus.Size.Y;

        }
        #endregion

        public override void Dispose()
        {
            ButtonMinus.Dispose();
            ButtonPlus.Dispose();
            ButtonThumb.Dispose();

            if (Buffer != null)
                Buffer.Dispose();
            
            if (Core.DebugLogging)
                Core.AddLogString("Dispose " + GetType().Name);
        }
        public override void TryCatchMouseInternal(Point ownerPos)
        {
            ButtonMinus.TryCatchMouse(ownerPos);
            ButtonPlus.TryCatchMouse(ownerPos);
            ButtonThumb.TryCatchMouse(ownerPos);
        }
        public override void Update()
        {
            if (IsEnabled && IsVisible)
            {
                if (Core.Control.LeftDown == ButtonThumb) // если таскаем ползунок
                {
                    if (Core.Control.LeftUpDown == ButtonThumb) // если только нажали, то запомним позицию курсора и ползунка
                    {
                        mouseCatchPos = Core.Input.MouseStateCurrent.Position;
                        if (orientation == Orientation.Horizontal)
                            mouseCatchThumbPos = ButtonThumb.Pos.X;
                        else
                            mouseCatchThumbPos = ButtonThumb.Pos.Y;
                    }

                    if (orientation == Orientation.Horizontal) // переместим ползунок
                        setThumbPos(mouseCatchThumbPos - (mouseCatchPos.X - Core.Input.MouseStateCurrent.X));
                    else
                        setThumbPos(mouseCatchThumbPos - (mouseCatchPos.Y - Core.Input.MouseStateCurrent.Y));

                    calcDataCurrent();
                }
                else if (Core.Control.LeftDown == ButtonMinus)
                {
                    repeatTimer.Update(Core.DtUpdate);

                    if (Core.Control.LeftUpDown == ButtonMinus)
                    {
                        repeatTimer.Stop();
                        repeatTimer.Start();
                        DataCurrent -= Step;
                    }
                    else
                    {
                        if (repeatTimer.OnOverflow)
                            DataCurrent -= Step;
                    }
                }
                else if (Core.Control.LeftDown == ButtonPlus)
                {
                    repeatTimer.Update(Core.DtUpdate);

                    if (Core.Control.LeftUpDown == ButtonPlus)
                    {
                        repeatTimer.Stop();
                        repeatTimer.Start();
                        DataCurrent += Step;
                    }
                    else
                    {
                        if (repeatTimer.OnOverflow)
                            DataCurrent += Step;
                    }
                }
                else if (Core.Control.LeftUpDown == this)
                {
                    if (orientation == Orientation.Horizontal)
                        setThumbPos(GetMousePosOnComponent().X - ButtonThumb.Size.X / 2);
                    else
                        setThumbPos(GetMousePosOnComponent().Y - ButtonThumb.Size.Y / 2);
                    calcDataCurrent();
                }
            }
        }
        public override void Layout()
        {
            if (IsInvalidateLayout)
            {
                if (orientation == Orientation.Horizontal)
                {
                    ButtonMinus.Size = new Point(Size.Y);
                    ButtonMinus.Pos = new Point(0);
                    ButtonPlus.Size = new Point(Size.Y);
                    ButtonPlus.Pos = new Point(Size.X - ButtonPlus.Size.X, 0);

                    if (Core.Control.LeftDown != ButtonThumb)
                    {
                        calcThumbSize();
                        setThumbPos(getThumbPos());
                    }

                    ButtonThumb.Size = new Point(ButtonThumb.Size.X, Size.Y);
                    ButtonThumb.Pos = new Point(ButtonThumb.Pos.X, 0);
                }
                else
                {
                    ButtonMinus.Size = new Point(Size.X);
                    ButtonMinus.Pos = new Point(0);
                    ButtonPlus.Size = new Point(Size.X);
                    ButtonPlus.Pos = new Point(0, Size.Y - ButtonPlus.Size.Y);

                    if (Core.Control.LeftDown != ButtonThumb)
                    {
                        calcThumbSize();
                        setThumbPos(getThumbPos());
                    }

                    ButtonThumb.Size = new Point(Size.X, ButtonThumb.Size.Y);
                    ButtonThumb.Pos = new Point(0, ButtonThumb.Pos.Y);
                }

            }
            ButtonThumb.Layout();
            ButtonPlus.Layout();
            ButtonMinus.Layout();

            if (IsInvalidateLayout)
            {
                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
        public override void Render()
        {
            ButtonThumb.Render(); // ошибкаа, вызов должен быть из секции IsInvalidateBuffer
            ButtonPlus.Render(); // Но не буду пока исправлять, возможно это останется стандартом
            ButtonMinus.Render(); // Чтобы поведение и реализация Invalidate Buffer и Layout были однотипны

            if (IsInvalidateBuffer)
            {
                PrepareBuffer();
                Core.Gd.SetRenderTarget(Buffer);
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                Core.Sb.Draw(BarTexture, new Rectangle(Point.Zero, Size), null, BarColor, 0f, Vector2.Zero, SpriteEffects.None, 0f);
                ButtonThumb.Draw();
                ButtonPlus.Draw();
                ButtonMinus.Draw();
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
