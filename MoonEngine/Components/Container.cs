﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Components
{
    abstract public class Container : Component
    {
        #region Стиль
        public class ContainerStyle : ComponentStyle
        {
            public ContainerStyle(Core core) : base(core)
            { }
            public ContainerStyle(Container component) : base(component)
            { }
        }

        public void SetStyle(ContainerStyle style)
        {
            base.SetStyle(style);
        }
        #endregion

        #region Свойства
        Component component = null;
        public Component Component
        {
            get { return component; }
            set
            {
                if (component != value)
                {
                    component = value;
                    if (Component != null) component.Owner = this;
                    InvalidateBuffer();
                    InvalidateLayout();
                }
            }
        }
        #endregion

        public Container(Core core, ContainerStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new ContainerStyle(Core));
            else
                SetStyle(style);
        }

        public override void Dispose()
        {
            if (Component != null)
                Component.Dispose();

            if (Buffer != null)
                Buffer.Dispose();
            
            if (Core.DebugLogging)
                Core.AddLogString("Dispose " + GetType().Name);
        }

        public override void TryCatchMouseInternal(Point ownerPos)
        {
            if (Component != null)
                Component.TryCatchMouse(ownerPos);
        }

        public override void Update()
        {
            if (IsEnabled)
                if (component != null)
                    component.Update();
        }

        public override void Layout()
        {
            if (component != null)
                component.Layout();

            if (IsInvalidateLayout)
            {
                // Расчёт шаблона
                // ...

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }

        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                if (component != null)
                    component.Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                // Отрисовка под component
                // ...
                if (component != null)
                    component.Draw();
                // Отрисовка над component
                // ...
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
