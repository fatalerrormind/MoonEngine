﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Components
{
    public class ContainerList : Component
    {
        #region Стиль
        public class ContainerListStyle : ComponentStyle
        {
            public ContainerListStyle(Core core) : base(core)
            { }
            public ContainerListStyle(ContainerList component) : base(component)
            { }
        }

        public void SetStyle(ContainerListStyle style)
        {
            base.SetStyle(style);
        }
        #endregion

        #region Свойства
        List<Component> components = new List<Component>();
        public List<Component> Components
        {
            get { return components; }
            private set
            {
                components = value;
                //if (components != value)
                //{
                //    if (value == null)
                //        components = new List<Component>();
                //    else
                //        components = value;
                //    InvalidateLayout();
                //    InvalidateBuffer();
                //    for (int i = 0; i < Components.Count; i++)
                //        Components[i].Owner = this;
                //}
            }
        } 
        #endregion

        public ContainerList(Core core, ContainerListStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new ContainerListStyle(Core));
            else
                SetStyle(style);
        }

        // Добавить компонент в лист и привязать к текущему, послать InvalidateLayout и InvalidateBuffer
        public void Bind(Component component)
        {
            Components.Add(component);
            component.Owner = this;

            InvalidateLayout();
            InvalidateBuffer();
        }
        // Удалить компонент из листа, послать InvalidateLayout и InvalidateBuffer
        public void Unbind(Component component, bool dispose)
        {
            for (int i = 0; i < Components.Count; i++)
                if (Components[i] == component)
                {
                    if (dispose)
                        Components[i].Dispose();
                    Components.RemoveAt(i--);

                    InvalidateLayout();
                    InvalidateBuffer();
                }
        }
        public void Unbind(int index, bool dispose)
        {
            if (dispose)
                Components[index].Dispose();
            Components.RemoveAt(index);

            InvalidateLayout();
            InvalidateBuffer();
        }
        // Переместить компонент на передний план
        public void ToFront(Component component)
        {
            for (int i = 0; i < Components.Count; i++)
                if (Components[i] == component)
                {
                    Components.Insert(Components.Count, Components[i]);
                    Components.RemoveAt(i);
                }
        }
        public void ToFront(int index)
        {
            Components.Insert(Components.Count, Components[index]);
            Components.RemoveAt(index);
        }
        // Переместить компонент на задний план
        public void ToBack(Component component)
        {
            for (int i = 0; i < Components.Count; i++)
                if (Components[i] == component)
                {
                    Components.Insert(0, Components[i]);
                    Components.RemoveAt(i + 1);
                }
        }
        public void ToBack(int index)
        {
            Components.Insert(0, Components[index]);
            Components.RemoveAt(index + 1);
        }

        public override void Dispose()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Dispose();

            if (Buffer != null)
                Buffer.Dispose();
            
            if (Core.DebugLogging)
                Core.AddLogString("Dispose " + GetType().Name);
        }

        public override void TryCatchMouseInternal(Point ownerPos)
        {
            for (int i = Components.Count - 1; i >= 0; i--)
                Components[i].TryCatchMouse(ownerPos);
        }

        public override void Update()
        {
            if (IsEnabled)
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Update();
        }
        public override void Layout()
        {
            for (int i = 0; i < Components.Count; i++)
                Components[i].Layout();

            if (IsInvalidateLayout)
            {
                // Расчёт шаблона
                // ...

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }

        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                // Отрисовка под Components
                // ...
                for (int i = 0; i < Components.Count; i++)
                    Components[i].Draw();
                // Отрисовка над Components
                // ...
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
