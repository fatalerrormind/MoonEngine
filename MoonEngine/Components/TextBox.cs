﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Components
{
    public class TextBox : Component
    {
        #region Свойства
        string text = "textBlock";
        public string Text
        {
            get { return text; }
            set { if (text != value) { text = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        SpriteFont font;
        public SpriteFont Font
        {
            get { return font; }
            set { if (font != value) { font = value; InvalidateBuffer(); InvalidateLayout(); } }
        }
        
        HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left;
        public HorizontalAlignment HorizontalAlignment
        {
            get { return horizontalAlignment; }
            set { if (horizontalAlignment != value) { horizontalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        VerticalAlignment verticalAlignment = VerticalAlignment.Top;
        public VerticalAlignment VerticalAlignment
        {
            get { return verticalAlignment; }
            set { if (verticalAlignment != value) { verticalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        Color colorEnabled = Color.Black;
        public Color ColorEnabled
        {
            get { return colorEnabled; }
            set { if (colorEnabled != value) { colorEnabled = value; InvalidateBuffer(); } }
        }

        Color colorDisabled = Color.Gray;
        public Color ColorDisabled
        {
            get { return colorDisabled; }
            set { if (colorDisabled != value) { colorDisabled = value; InvalidateBuffer(); } }
        }
        #endregion

        public TextBox(Core core, string text, SpriteFont font) : base(core)
        {
            this.text = text;
            this.font = font;
        }
        public TextBox(Core core) : base(core)
        {
        }
        
        public override void Layout()
        {
            if (IsInvalidateLayout)
            {


                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }

        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);

                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
