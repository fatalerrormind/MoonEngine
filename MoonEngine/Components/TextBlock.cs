﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System;
using System.Text;

namespace MoonEngine.Components
{
    public class TextBlock : Component
    {
        string[] textDraw;

        #region Свойства
        string text = "textBlock";
        public string Text
        {
            get { return text; }
            set { if (text != value) { text = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        SpriteFont font;
        public SpriteFont Font
        {
            get { return font; }
            set { if (font != value) { font = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        HorizontalAlignment horizontalAlignment = HorizontalAlignment.Left;
        public HorizontalAlignment HorizontalAlignment
        {
            get { return horizontalAlignment; }
            set { if (horizontalAlignment != value) { horizontalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        Color textColor = Color.Black;
        public Color TextColor
        {
            get { return textColor; }
            set { if (textColor != value) { textColor = value; InvalidateBuffer(); } }
        }
        #endregion

        public TextBlock(Core core, string text, SpriteFont font) : base(core)
        {
            this.text = text;
            this.font = font;
        }
        public TextBlock(Core core) : base(core)
        { }

        public override void Layout()
        {
            if(IsInvalidateLayout)
            {
                //StringBuilder strBuilder = new StringBuilder(text);
                //for (int c = 0; c < strBuilder.Length; c++) // c - char - символ
                //{ }

                List<string> strings = new List<string>();
                string[] paragraphs = text.Split('\n'); // бьём на абзацы

                for (int p = 0; p < paragraphs.Length; p++)
                {
                    strings.Add("");
                    string[] words = paragraphs[p].Split(' ');

                    for (int w = 0; w < words.Length;)
                    {
                        bool space = strings[strings.Count - 1].Length != 0;

                        if (font.MeasureString(strings[strings.Count - 1]).X + font.MeasureString((space ? " " : "") + words[w]).X <= Size.X || !space)
                        {
                            if (space)
                                strings[strings.Count - 1] += " ";
                            strings[strings.Count - 1] += words[w];
                            w++;
                        }
                        else
                            strings.Add("");
                    }
                }

                Size = new Point(Size.X, (int)(font.MeasureString(" ").Y * strings.Count));
                textDraw = strings.ToArray();
                strings.Clear();

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }

        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                float allY = 0f;
                for (int i = 0; i < textDraw.Length; i++)
                {
                    Vector2 strSize = font.MeasureString(textDraw[i]);
                    float posX;

                    if (horizontalAlignment == HorizontalAlignment.Left)
                        posX = 0;
                    else if (horizontalAlignment == HorizontalAlignment.Center)
                        posX = (Size.X / 2) - strSize.X / 2;
                    else
                        posX = Size.X - strSize.X;

                    Core.Sb.DrawString(font, textDraw[i], new Vector2(posX, i * strSize.Y), textColor);
                    allY += strSize.Y;
                }
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
