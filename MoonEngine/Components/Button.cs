﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MoonEngine.Components
{
    public class Button : Container
    {
        #region Стиль
        public class ButtonStyle : ContainerStyle
        {
            public HorizontalAlignment HorizontalAlignment { get; set; }
            public VerticalAlignment VerticalAlignment { get; set; }
            public Color ColorNormal { get; set; }
            public Color ColorHover { get; set; }
            public Color ColorPressed { get; set; }
            public Texture2D TextureNormal { get; set; }
            public Texture2D TextureHover { get; set; }
            public Texture2D TexturePressed { get; set; }
            public bool FrameIsVisible { get; set; }
            public byte FrameWidth { get; set; }
            public Color FrameColorNormal { get; set; }
            public Color FrameColorHover { get; set; }
            public Color FrameColorPressed { get; set; }
            public bool MaskIsVisible { get; set; }
            public Color MaskColorNormal { get; set; }
            public Color MaskColorHover { get; set; }
            public Color MaskColorPressed { get; set; }
            public Texture2D MaskTextureNormal { get; set; }
            public Texture2D MaskTextureHover { get; set; }
            public Texture2D MaskTexturePressed { get; set; }

            public ButtonStyle(Core core) : base(core)
            {
                IsClickable = true;
                IsVisualReactionOnMouse = true;
                IsVisualReactionIsLeftDown = true;
                IsVisualReactionIsRightDown = true;
                Size = new Point(50, 20);

                HorizontalAlignment = HorizontalAlignment.Center;
                VerticalAlignment = VerticalAlignment.Center;
                ColorNormal = Color.LightGray;
                ColorHover = Color.LightGray;
                ColorPressed = Color.LightGray;
                TextureNormal = core.t2d_white_pixel; // ссылка
                TextureHover = core.t2d_white_pixel; // ссылка
                TexturePressed = core.t2d_white_pixel; // ссылка
                FrameIsVisible = true;
                FrameWidth = 1;
                FrameColorNormal = Color.Gray;
                FrameColorHover = Color.Red;
                FrameColorPressed = Color.DarkRed;
                MaskIsVisible = true;
                MaskColorNormal = Color.Transparent;
                MaskColorHover = Color.FromNonPremultiplied(255, 255, 255, 100);
                MaskColorPressed = Color.FromNonPremultiplied(0, 0, 0, 100);
                MaskTextureNormal = core.t2d_white_pixel; // ссылка
                MaskTextureHover = core.t2d_white_pixel; // ссылка
                MaskTexturePressed = core.t2d_white_pixel; // ссылка
            }
            public ButtonStyle(Button component) : base(component)
            {
                HorizontalAlignment = component.HorizontalAlignment;
                VerticalAlignment = component.VerticalAlignment;
                ColorNormal = component.ColorNormal;
                ColorHover = component.ColorHover;
                ColorPressed = component.ColorPressed;
                TextureNormal = component.TextureNormal; // ссылка
                TextureHover = component.TextureHover; // ссылка
                TexturePressed = component.TexturePressed; // ссылка
                FrameIsVisible = component.FrameIsVisible;
                FrameWidth = component.FrameWidth;
                FrameColorNormal = component.FrameColorNormal;
                FrameColorHover = component.FrameColorHover;
                FrameColorPressed = component.FrameColorPressed;
                MaskIsVisible = component.MaskIsVisible;
                MaskColorNormal = component.MaskColorNormal;
                MaskColorHover = component.MaskColorHover;
                MaskColorPressed = component.MaskColorPressed;
                MaskTextureNormal = component.MaskTextureNormal; // ссылка
                MaskTextureHover = component.MaskTextureHover; // ссылка
                MaskTexturePressed = component.MaskTexturePressed; // ссылка
            }
        }

        public void SetStyle(ButtonStyle style)
        {
            base.SetStyle(style);
            HorizontalAlignment = style.HorizontalAlignment;
            VerticalAlignment = style.VerticalAlignment;
            ColorNormal = style.ColorNormal;
            ColorHover = style.ColorHover;
            ColorPressed = style.ColorPressed;
            TextureNormal = style.TextureNormal; // ссылка
            TextureHover = style.TextureHover; // ссылка
            TexturePressed = style.TexturePressed; // ссылка
            FrameIsVisible = style.FrameIsVisible;
            FrameWidth = style.FrameWidth;
            FrameColorNormal = style.FrameColorNormal;
            FrameColorHover = style.FrameColorHover;
            FrameColorPressed = style.FrameColorPressed;
            MaskIsVisible = style.MaskIsVisible;
            MaskColorNormal = style.MaskColorNormal;
            MaskColorHover = style.MaskColorHover;
            MaskColorPressed = style.MaskColorPressed;
            MaskTextureNormal = style.MaskTextureNormal; // ссылка
            MaskTextureHover = style.MaskTextureHover; // ссылка
            MaskTexturePressed = style.MaskTexturePressed; // ссылка
        }
        #endregion
        #region Свойства
        HorizontalAlignment horizontalAlignment;
        public HorizontalAlignment HorizontalAlignment
        {
            get { return horizontalAlignment; }
            set { if (horizontalAlignment != value) { horizontalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        VerticalAlignment verticalAlignment;
        public VerticalAlignment VerticalAlignment
        {
            get { return verticalAlignment; }
            set { if (verticalAlignment != value) { verticalAlignment = value; InvalidateBuffer(); InvalidateLayout(); } }
        }

        Color colorNormal;
        public Color ColorNormal
        {
            get { return colorNormal; }
            set { if (colorNormal != value) { colorNormal = value; InvalidateBuffer(); } }
        }

        Color colorHover;
        public Color ColorHover
        {
            get { return colorHover; }
            set { if (colorHover != value) { colorHover = value; InvalidateBuffer(); } }
        }

        Color colorPressed;
        public Color ColorPressed
        {
            get { return colorPressed; }
            set { if (colorPressed != value) { colorPressed = value; InvalidateBuffer(); } }
        }

        Texture2D textureNormal;
        public Texture2D TextureNormal
        {
            get { return textureNormal; }
            set { if (textureNormal != value) { textureNormal = value; InvalidateBuffer(); } }
        }

        Texture2D textureHover;
        public Texture2D TextureHover
        {
            get { return textureHover; }
            set { if (textureHover != value) { textureHover = value; InvalidateBuffer(); } }
        }

        Texture2D texturePressed;
        public Texture2D TexturePressed
        {
            get { return texturePressed; }
            set { if (texturePressed != value) { texturePressed = value; InvalidateBuffer(); } }
        }

        bool frameIsVisible;
        public bool FrameIsVisible
        {
            get { return frameIsVisible; }
            set { if (frameIsVisible != value) { frameIsVisible = value; InvalidateBuffer(); } }
        }

        byte frameWidth;
        public byte FrameWidth
        {
            get { return frameWidth; }
            set { if (frameWidth != value) { frameWidth = value; InvalidateBuffer(); } }
        }

        Color frameColorNormal;
        public Color FrameColorNormal
        {
            get { return frameColorNormal; }
            set { if (frameColorNormal != value) { frameColorNormal = value; InvalidateBuffer(); } }
        }

        Color frameColorHover;
        public Color FrameColorHover
        {
            get { return frameColorHover; }
            set { if (frameColorHover != value) { frameColorHover = value; InvalidateBuffer(); } }
        }

        Color frameColorPressed;
        public Color FrameColorPressed
        {
            get { return frameColorPressed; }
            set { if (frameColorPressed != value) { frameColorPressed = value; InvalidateBuffer(); } }
        }

        bool maskIsVisible;
        public bool MaskIsVisible
        {
            get { return maskIsVisible; }
            set { if (maskIsVisible != value) { maskIsVisible = value; InvalidateBuffer(); } }
        }

        Color maskColorNormal;
        public Color MaskColorNormal
        {
            get { return maskColorNormal; }
            set { if (maskColorNormal != value) { maskColorNormal = value; InvalidateBuffer(); } }
        }

        Color maskColorHover;
        public Color MaskColorHover
        {
            get { return maskColorHover; }
            set { if (maskColorHover != value) { maskColorHover = value; InvalidateBuffer(); } }
        }

        Color maskColorPressed;
        public Color MaskColorPressed
        {
            get { return maskColorPressed; }
            set { if (maskColorPressed != value) { maskColorPressed = value; InvalidateBuffer(); } }
        }

        Texture2D maskTextureNormal;
        public Texture2D MaskTextureNormal
        {
            get { return maskTextureNormal; }
            set { if (maskTextureNormal != value) { maskTextureNormal = value; InvalidateBuffer(); } }
        }

        Texture2D maskTextureHover;
        public Texture2D MaskTextureHover
        {
            get { return maskTextureHover; }
            set { if (maskTextureHover != value) { maskTextureHover = value; InvalidateBuffer(); } }
        }

        Texture2D maskTexturePressed;
        public Texture2D MaskTexturePressed
        {
            get { return maskTexturePressed; }
            set { if (maskTexturePressed != value) { maskTexturePressed = value; InvalidateBuffer(); } }
        }
        #endregion

        public Button(Core core, string labelText, Label.LabelStyle labelStyle = null, ButtonStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new ButtonStyle(Core));
            else
                SetStyle(style);

            Label label = new Label(core, labelText, labelStyle);
            Component = label;
        }
        public Button(Core core, Component component, ButtonStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new ButtonStyle(Core));
            else
                SetStyle(style);

            Component = component;
        }
        public Button(Core core, ButtonStyle style = null) : base(core)
        {
            if (style == null)
                SetStyle(new ButtonStyle(Core));
            else
                SetStyle(style);
        }

        public override void Update()
        {
            if (IsEnabled)
                if (Component != null)
                    Component.Update();
        }
        public override void Layout()
        {
            if (Component != null)
                Component.Layout();

            if (IsInvalidateLayout)
            {
                if (Component != null)
                {
                    if (HorizontalAlignment == HorizontalAlignment.Left)
                        Component.Pos = new Point(Component.Margin.Left, Component.Pos.Y);
                    else if (HorizontalAlignment == HorizontalAlignment.Center)
                        Component.Pos = new Point(Size.X / 2 - (Component.Margin.Left + Component.Size.X + Component.Margin.Right) / 2, Component.Pos.Y);
                    else
                        Component.Pos = new Point(Size.X - Component.Size.X - Component.Margin.Right, Component.Pos.Y);
                    
                    if (VerticalAlignment == VerticalAlignment.Top)
                        Component.Pos = new Point(Component.Pos.X, Component.Margin.Top);
                    else if (VerticalAlignment == VerticalAlignment.Center)
                        Component.Pos = new Point(Component.Pos.X, Size.Y / 2 - (Component.Margin.Top + Component.Size.Y + Component.Margin.Bottom) / 2);
                    else
                        Component.Pos = new Point(Component.Pos.X, Size.Y - Component.Size.Y - Component.Margin.Bottom);
                }

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                if (Component != null)
                    Component.Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointWrap);
                if ((Core.Control.LeftDown == this && IsVisualReactionIsLeftDown) || (Core.Control.RightDown == this && IsVisualReactionIsRightDown))
                {
                    Core.Sb.Draw(TexturePressed, new Rectangle(Point.Zero, Size), null, ColorPressed, 0f, Vector2.Zero, SpriteEffects.None, 0f);

                    if (Component != null)
                        Component.Draw();

                    if (MaskIsVisible)
                        Core.Sb.Draw(MaskTexturePressed, new Rectangle(Point.Zero, Size), null, MaskColorPressed, 0f, Vector2.Zero, SpriteEffects.None, 0f);

                    if (FrameIsVisible && FrameWidth > 0)
                        Core.Sb.DrawRectangleСontour(Point.Zero, Size, FrameWidth, FrameColorPressed);
                }
                else if (Core.Control.OnMouse == this && IsVisualReactionOnMouse)
                {
                    Core.Sb.Draw(TextureHover, new Rectangle(Point.Zero, Size), null, ColorHover, 0f, Vector2.Zero, SpriteEffects.None, 0f);

                    if (Component != null)
                        Component.Draw();

                    if (MaskIsVisible)
                        Core.Sb.Draw(MaskTextureHover, new Rectangle(Point.Zero, Size), null, MaskColorHover, 0f, Vector2.Zero, SpriteEffects.None, 0f);

                    if (FrameIsVisible && FrameWidth > 0)
                        Core.Sb.DrawRectangleСontour(Point.Zero, Size, FrameWidth, FrameColorHover);
                }
                else
                {
                    Core.Sb.Draw(TextureNormal, new Rectangle(Point.Zero, Size), null, ColorNormal, 0f, Vector2.Zero, SpriteEffects.None, 0f);

                    if (Component != null)
                        Component.Draw();

                    if (MaskIsVisible)
                        Core.Sb.Draw(MaskTextureNormal, new Rectangle(Point.Zero, Size), null, MaskColorNormal, 0f, Vector2.Zero, SpriteEffects.None, 0f);

                    if (FrameIsVisible && FrameWidth > 0)
                        Core.Sb.DrawRectangleСontour(Point.Zero, Size, FrameWidth, FrameColorNormal);
                }
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
