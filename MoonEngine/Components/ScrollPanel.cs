﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonEngine.Components
{
    public class ScrollPanel : Container
    {
        #region Стиль
        public class ScrollPanelStyle : ContainerStyle
        {
            public ScrollBar.ScrollBarStyle ScrollBarHorizontalStyle { get; set; }
            public ScrollBar.ScrollBarStyle ScrollBarVerticalStyle { get; set; }
            public Button.ButtonStyle StubStyle { get; set; }

            public ScrollPanelStyle(Core core) : base(core)
            {
                Size = new Point(40);
                ScrollBarHorizontalStyle = new ScrollBar.ScrollBarStyle(core);
                ScrollBarVerticalStyle = new ScrollBar.ScrollBarStyle(core);
                StubStyle = new Button.ButtonStyle(core);
            }
            public ScrollPanelStyle(ScrollPanel component) : base(component)
            {
                ScrollBarHorizontalStyle = new ScrollBar.ScrollBarStyle(component.ScrollBarHorizontal);
                ScrollBarVerticalStyle = new ScrollBar.ScrollBarStyle(component.ScrollBarVertical);
                StubStyle = new Button.ButtonStyle(component.Stub);
            }
        }

        public void SetStyle(ScrollPanelStyle style)
        {
            base.SetStyle(style);
            ScrollBarHorizontal.SetStyle(style.ScrollBarHorizontalStyle);
            ScrollBarVertical.SetStyle(style.ScrollBarVerticalStyle);
            Stub.SetStyle(style.StubStyle);

            ScrollBarHorizontal.Orientation = Orientation.Horizontal;
            ScrollBarVertical.Orientation = Orientation.Vertical;
        }
        #endregion
        #region Свойства
        public ScrollBar ScrollBarHorizontal { get; }
        public ScrollBar ScrollBarVertical { get; }
        public Button Stub { get; }
        #endregion

        ScrollBarsTemplate scrollBarsTemplate;

        public ScrollPanel(Core core, ScrollPanelStyle style = null) : base(core)
        {
            ScrollBarHorizontal = new ScrollBar(Core);
            ScrollBarHorizontal.Owner = this;

            ScrollBarVertical = new ScrollBar(Core);
            ScrollBarVertical.Owner = this;

            Stub = new Button(Core);
            Stub.Owner = this;

            scrollBarsTemplate = new ScrollBarsTemplate();
            scrollBarsTemplate.Pos = Point.Zero;
            scrollBarsTemplate.ScrollBarHorizontal = ScrollBarHorizontal;
            scrollBarsTemplate.ScrollBarVertical = ScrollBarVertical;
            scrollBarsTemplate.Stub = Stub;

            if (style == null)
                SetStyle(new ScrollPanelStyle(Core));
            else
                SetStyle(style);
        }

        public override void Dispose()
        {
            if (Component != null)
                Component.Dispose();

            ScrollBarVertical.Dispose();
            ScrollBarHorizontal.Dispose();
            Stub.Dispose();

            if (Buffer != null)
                Buffer.Dispose();

            if (Core.DebugLogging)
                Core.AddLogString("Dispose " + GetType().Name);
        }
        public override void TryCatchMouseInternal(Point ownerPos)
        {
            ScrollBarVertical.TryCatchMouse(ownerPos);
            ScrollBarHorizontal.TryCatchMouse(ownerPos);
            Stub.TryCatchMouse(ownerPos);

            if (Component != null)
                Component.TryCatchMouse(ownerPos);
        }
        public override void Update()
        {
            if (IsEnabled)
            {
                if (Component != null)
                    Component.Update();

                ScrollBarVertical.Update();
                ScrollBarHorizontal.Update();
                Stub.Update();
            }
        }
        public override void Layout()
        {
            if (Component != null)
                Component.Layout();

            if (IsInvalidateLayout)
            {
                // Установить размер данных
                if (Component != null)
                {
                    ScrollBarVertical.DataSize = Component.Size.Y;
                    ScrollBarHorizontal.DataSize = Component.Size.X;
                }
                else
                {
                    ScrollBarVertical.DataSize = 0;
                    ScrollBarHorizontal.DataSize = 0;
                }
                // Установить размер области для шаблона и рассчитать размеры скроллбаров черех шаблон
                scrollBarsTemplate.Size = Size;
                scrollBarsTemplate.CalcSize();

                // Установить области просмотра
                if (Stub.IsVisible && (!ScrollBarVertical.IsVisible || !ScrollBarHorizontal.IsVisible))
                {
                    ScrollBarVertical.ViewSize = ScrollBarVertical.Size.Y + Stub.Size.Y;
                    ScrollBarHorizontal.ViewSize = ScrollBarHorizontal.Size.X + Stub.Size.X;
                }
                else
                {
                    ScrollBarVertical.ViewSize = ScrollBarVertical.Size.Y;
                    ScrollBarHorizontal.ViewSize = ScrollBarHorizontal.Size.X;
                }
            }

            ScrollBarVertical.Layout();
            ScrollBarHorizontal.Layout();
            Stub.Layout();

            if (IsInvalidateLayout)
            {
                // Рассчитать позиции скроллбарам через шаблон
                scrollBarsTemplate.CalcPos();
                // Установить компоненту позицию из данных скроллбара
                if (Component != null)
                    Component.Pos = new Point(-(int)ScrollBarHorizontal.DataCurrent, -(int)ScrollBarVertical.DataCurrent);

                IsInvalidateLayout = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Layout " + GetType().Name);
            }
        }
        public override void Render()
        {
            if (IsInvalidateBuffer)
            {
                if (Component != null)
                    Component.Render();

                ScrollBarVertical.Render();
                ScrollBarHorizontal.Render();
                Stub.Render();

                PrepareBuffer();
                Core.Sb.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
                if (Component != null)
                    Component.Draw();
                ScrollBarVertical.Draw();
                ScrollBarHorizontal.Draw();
                Stub.Draw();
                Core.Sb.End();

                IsInvalidateBuffer = false;
                if (Core.DebugLogging)
                    Core.AddLogString("Render " + GetType().Name);
            }
        }
    }
}
