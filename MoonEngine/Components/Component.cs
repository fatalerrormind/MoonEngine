﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MoonEngine.Modules;
using System;

namespace MoonEngine.Components
{
    public enum HorizontalAlignment
    {
        Left, Center, Right
    }
    public enum VerticalAlignment
    {
        Top, Center, Bottom
    }
    public enum Orientation
    {
        Horizontal, Vertical
    }
    public enum Corner
    {
        TopLeft, TopCenter, TopRight,
        CenterLeft, CenterCenter, CenterRight,
        BottomLeft, BottomCenter, BottomRight
    }
    public enum SelectMode
    { None, Single, Multiple }
    //public delegate void EventDelegate();

    abstract public class Component : IDisposable
    {
        #region Стиль
        public class ComponentStyle
        {
            public bool IsClickable { get; set; }
            public bool IsVisualReactionOnMouse { get; set; }
            public bool IsVisualReactionIsLeftDown { get; set; }
            public bool IsVisualReactionIsRightDown { get; set; }
            public bool IsVisible { get; set; }
            public bool IsEnabled { get; set; }
            public Color ClearColor { get; set; }
            public Color EnabledTone { get; set; }
            public Color DisabledTone { get; set; }
            public Margin Margin { get; set; }
            public Point Size { get; set; }
            public Point Pos { get; set; }

            public ComponentStyle(Core core)
            {
                IsClickable = false;
                IsVisualReactionOnMouse = false;
                IsVisualReactionIsLeftDown = false;
                IsVisualReactionIsRightDown = false;
                IsVisible = true;
                IsEnabled = true;
                ClearColor = Color.Transparent;
                EnabledTone = new Color(255, 255, 255, 255);
                DisabledTone = new Color(200, 128, 128, 255);
                Margin = Margin.Zero;
                Size = new Point(1, 1);
                Pos = new Point(0, 0);
            }
            public ComponentStyle(Component component)
            {
                IsClickable = component.CanCatchMouse;
                IsVisualReactionOnMouse = component.IsVisualReactionOnMouse;
                IsVisualReactionIsLeftDown = component.IsVisualReactionIsLeftDown;
                IsVisualReactionIsRightDown = component.IsVisualReactionIsRightDown;
                IsVisible = component.IsVisible;
                IsEnabled = component.IsEnabled;
                ClearColor = component.ClearColor;
                EnabledTone = component.EnabledTone;
                DisabledTone = component.DisabledTone;
                Margin = component.Margin;
                Size = component.Size;
                Pos = component.Pos;
            }
        }
        
        public void SetStyle(ComponentStyle style)
        {
            CanCatchMouse = style.IsClickable;
            IsVisualReactionOnMouse = style.IsVisualReactionOnMouse;
            IsVisualReactionIsLeftDown = style.IsVisualReactionIsLeftDown;
            IsVisualReactionIsRightDown = style.IsVisualReactionIsRightDown;
            IsVisible = style.IsVisible;
            IsEnabled = style.IsEnabled;
            ClearColor = style.ClearColor;
            EnabledTone = style.EnabledTone;
            DisabledTone = style.DisabledTone;
            Margin = style.Margin;
            Size = style.Size;
            Pos = style.Pos;
        }
        #endregion
        #region Системные свойства
        // Ядро на базе которого работает компонент
        public Core Core { get; }
        // Компонент-владелец компонента, если таковой есть (есть если компонент вложен в другой компонент)
        public Component Owner { get; set; }
        // Буфер для рендеринга
        public RenderTarget2D Buffer { get; protected set; }
        // Источник графического представления, часть буфера, которая будет отрисована
        //Rectangle source = Rectangle.Empty;
        //virtual public Rectangle Source
        //{
        //    get { return source; }
        //    set
        //    {
        //        if (source != value)
        //        {
        //            source = value;
        //            if (Owner != null) Owner.InvalidateBuffer();
        //        }
        //    }
        //}
        #endregion
        #region Свойства
        // Отклик на курсор мыши
        virtual public bool CanCatchMouse { get; set; } // Реагировать на курсор или игнорировать его
        virtual public bool IsVisualReactionOnMouse { get; set; } // Нужна ли визуальная реакция на наведение курсора (будет вызываться InvalidateBuffer() при изменении свойства OnMouse для текущего компонента)
        virtual public bool IsVisualReactionIsLeftDown { get; set; } // Визуальная реакция на нажатие левой кнопки мыши на компоненте (будет вызываться InvalidateBuffer() при изменении свойства IsLeftDown для текущего компонента)
        virtual public bool IsVisualReactionIsRightDown { get; set; } // Визуальная реакция на нажатие правой кнопки мыши (будет вызываться InvalidateBuffer() при изменении свойства IsRightDown для текущего компонента)

        virtual public bool IsInvalidateBuffer { get; set; } // Флаг сообщает, нужно ли перерисовать компонент в буфер на текущем кадре
        virtual public bool IsInvalidateLayout { get; set; } // Флаг сообщает, нужно ли обновить компановку компонента на текущем кадре

        // Видимый ли компонент
        bool isVisible;
        virtual public bool IsVisible
        {
            get { return isVisible; }
            set
            {
                if (isVisible != value)
                {
                    isVisible = value;
                    if (Owner != null) { Owner.InvalidateLayout(); Owner.InvalidateBuffer(); }
                }
            }
        }

        // Активен ли компонент
        bool isEnabled;
        virtual public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    InvalidateBuffer();
                }
            }
        }

        // Цвет очистки буфера компонента перед отрисовкой
        Color clearColor;
        virtual public Color ClearColor
        {
            get { return clearColor; }
            set { if (clearColor != value) { clearColor = value; InvalidateBuffer(); } }
        }

        // Цвет тонирования компонента если тот активен
        Color enabledTone;
        virtual public Color EnabledTone
        {
            get { return enabledTone; }
            set { if (enabledTone != value) { enabledTone = value; InvalidateBuffer(); } }
        }

        // Цвет тонирования компонента если тот не активен
        Color disabledTone;
        virtual public Color DisabledTone
        {
            get { return disabledTone; }
            set { if (disabledTone != value) { disabledTone = value; InvalidateBuffer(); } }
        }

        // Интервалы со всех сторон компонента, эти данные учитывают некоторые другие компоненты с автоматической компановкой
        Margin margin;
        virtual public Margin Margin
        {
            get { return margin; }
            set
            {
                if (margin != value)
                {
                    margin = value;
                    if (Owner != null) Owner.InvalidateBuffer();
                }
            }
        }

        // Размер компонента, а так же размер его буфера
        Point size;
        virtual public Point Size
        {
            get { return size; }
            set
            {
                if (size != value)
                {
                    size = value;
                    InvalidateBuffer();
                    InvalidateLayout();
                    if (Owner != null) Owner.InvalidateLayout();

                    if (size.X < 1) size.X = 1;
                    if (size.Y < 1) size.Y = 1;
                    //SizeChanged?.Invoke();
                }
            }
        }
        //public event EventDelegate SizeChanged;

        // Позиция компонента относительно его родителя (формы или другого компонента)
        Point pos;
        virtual public Point Pos
        {
            get { return pos; }
            set
            {
                if (pos != value)
                {
                    pos = value;
                    if (Owner != null) { Owner.InvalidateBuffer(); Owner.InvalidateLayout(); }
                    //PosChanged?.Invoke();
                }
            }
        }
        //public event EventDelegate PosChanged;
        #endregion

        public Component(Core core, ComponentStyle style = null)
        {
            Core = core;

            if (style == null)
                SetStyle(new ComponentStyle(Core));
            else
                SetStyle(style);
        }

        // Метод высвобождает память выделенную буферу, нужно вызывать если компонент больше не будет использован
        virtual public void Dispose()
        {
            if (Buffer != null)
                Buffer.Dispose();
            
            if (Core.DebugLogging)
                Core.AddLogString("Dispose " + GetType().Name);
        }

        // Устанавливает флаг невалидности графического представления
        virtual public void InvalidateBuffer()
        {
            IsInvalidateBuffer = true;
            if (Owner != null) Owner.InvalidateBuffer();
        }

        // Устанавливает флаг невалидности макета
        virtual public void InvalidateLayout()
        {
            IsInvalidateLayout = true;
        }

        // Получить позицию на экране (в экранной системе координат выраженной в пикселях)
        virtual public Point GetPosOnScreen()
        {
            Point p = Point.Zero; ;
            for (Component c = this; c != null; c = c.Owner)
                p = p + c.Pos /*- c.Source.Location*/;
            return p;
        }

        // Получить позицию мыши относительно левого верхнего угла компонента
        virtual public Point GetMousePosOnComponent()
        {
            Point p = Point.Zero;
            for (Component c = this; c != null; c = c.Owner)
                    p = p + c.Pos /*- c.Source.Location*/;
            return Core.Input.MouseStateCurrent.Position - p;
        }

        // Проверить, находится ли курсор на компоненте
        // Cамостоятельный вызов может выдавать неверное значение при перекрытии компонента другим
        // Позиция получается в случае автоматического вызова путём проброса позиций через стек вызовов метода TryCatchMouse
        // или при самостоятельном вызове можно воспользоваться методом GetPosOnScreen()
        virtual public bool CheckOnMouse(Point p)
        {
            //return Core.Control.MouseOnRect(p, Source == Rectangle.Empty ? Size : new Point(MathHelper.Min(Size.X - Source.X, Source.Size.X), MathHelper.Min(Size.Y - Source.Y, Source.Size.Y)));
            return Core.Input.MouseOnRect(p, Size);
        }

        // Попытка захватить курсор мыши
        virtual public void TryCatchMouse(Point ownerPos)
        {
            ownerPos += Pos;

            if (IsVisible)
                if (CheckOnMouse(ownerPos))
                {
                    if (IsEnabled)
                    {
                        //ownerPos -= Source.Location;
                        TryCatchMouseInternal(ownerPos);
                    }

                    if (CanCatchMouse)
                    {
                        if (!Core.Control.MouseCatch)
                        {
                            Core.Control.MouseCatch = true;
                            if (IsEnabled)
                                Core.Control.OnMouse = this;
                        }
                    }
                }
        }

        // Попытка захватит курсор вложенными компонентами или прочими элементами управления
        virtual public void TryCatchMouseInternal(Point ownerPos)
        { }

        // Обновление, просчёт логики, реакция на пользовательский ввод
        virtual public void Update()
        { }

        // Компановка внутренних компонентов и прочих элементов
        virtual public void Layout()
        {
            IsInvalidateLayout = false;
        }

        // Отрисовка компонента в свой буфер
        virtual public void Render()
        {
            IsInvalidateBuffer = false;
        }

        // Отрисовка буфера компонента, вызывается между методами SpriteBatch.Begin() и SpriteBatch.End()
        virtual public void Draw()
        {
            if (IsVisible)
            {
                //Rectangle sourceCorrect = Rectangle.Empty;

                //if (Source == Rectangle.Empty)
                //    sourceCorrect = new Rectangle(0, 0, Size.X, Size.Y);
                //else
                //{
                //    sourceCorrect = Source;
                //    if (sourceCorrect.X >= Size.X) sourceCorrect.X = Size.X - 1; // коррекция позиции соурса
                //    if (sourceCorrect.Y >= Size.Y) sourceCorrect.Y = Size.Y - 1;
                //    if (sourceCorrect.X + sourceCorrect.Width > Size.X) sourceCorrect.Width = Size.X - sourceCorrect.X; // коррекция размера соурса
                //    if (sourceCorrect.Y + sourceCorrect.Height > Size.Y) sourceCorrect.Height = Size.Y - sourceCorrect.Y;
                //}

                //Core.Sb.Draw(Buffer, Pos.ToVector2(), sourceCorrect, IsEnabled ? EnabledTone : DisabledTone);
                Core.Sb.Draw(Buffer, Pos.ToVector2(), IsEnabled ? EnabledTone : DisabledTone);
            }
        }

        // Подготовка буфера, коррекция размера, очистка
        protected void PrepareBuffer()
        {
            if (Buffer == null)
                Buffer = new RenderTarget2D(Core.Gd, Size.X, Size.Y);
            else if (Buffer.Width != Size.X || Buffer.Height != Size.Y || Buffer.IsDisposed)
            {
                Buffer.Dispose();
                Buffer = new RenderTarget2D(Core.Gd, Size.X, Size.Y);
            }

            Core.Gd.SetRenderTarget(Buffer);
            Core.Gd.Clear(ClearColor);
        }
    }
}
